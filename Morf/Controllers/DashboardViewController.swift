
import UIKit

class DashboardViewController: MorfBaseController, UITableViewDataSource, DashboardCellDelegate {

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! DashboardCell
        cell.delegate = self
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func onlinePlayerSelected(name: String) {
        self.tabBarController?.selectedIndex = 2
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        let newDashboardVC = self.storyboard?.instantiateInitialViewController() as UIViewController?
        self.navigationController!.viewControllers[0] = newDashboardVC!
    }
    
}

//
//  MorfTabBarController.h
//  Morf
//
//  Created by Tushar on 01/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MorfTabBarController : UITabBarController

@property (nonatomic) NSUInteger defaultSelectedIndex;

@end


import UIKit
import AudioToolbox

class CourseCompletionViewController: BaseMediaPlayerController, UITableViewDataSource {

    var course: Course!
    var chapter: Chapter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPlayBackFileAndPlay("CourseComplete.mp3")
        self.navigationItem.hidesBackButton = true
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CourseCompletionCell") as! CourseCompletionCell
        cell.showCompletionInfo(course)
        return cell
    }
    
    override func backTapped() {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
}

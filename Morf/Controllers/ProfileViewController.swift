

import UIKit

class ProfileViewController: BaseMediaPlayerController, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationItem.hidesBackButton = true
        tableView.reloadData()
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ProfileCell") as! ProfileCell
        cell.update(AppDelegate.currentUser())
        return cell
    }
    
    func playSound() {
        setPlayBackFileAndPlay("CourseComplete.mp3")
    }
}

//
//  ChaptersListingControllerViewController.h
//  Morf
//
//  Created by Tushar on 04/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "BaseMediaPlayerController.h"
#import "BaseTTSViewController.h"
@class Course;

@interface ChaptersListingController : BaseTTSViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) Course *course;

@end

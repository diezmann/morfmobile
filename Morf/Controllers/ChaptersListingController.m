//
//  ChaptersListingControllerViewController.m
//  Morf
//
//  Created by Tushar on 04/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "ChaptersListingController.h"
#import "Course.h"
#import "ChapterListingCell.h"
#import "Morf-Swift.h"
#import "UIBarButtonItem+MorfAdditions.h"

@interface ChaptersListingController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *siriPlayerView;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end

@implementation ChaptersListingController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.userName.text = [AppDelegate currentUser].teamName;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.scoreLabel.text = [NSString stringWithFormat:@" %ld Points", [AppDelegate currentUser].teamPoints];
    [self.siriPlayerView addSubview:self.mediaPlayerView];
    self.backgroundImageView.image = [UIImage imageNamed:@"Bg_with_teaching_image"];
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    if(self.course && self.course.chapters.count > 0) {
        BOOL isIncompleFound = NO;
        for(Chapter *chapter in self.course.chapters) {
            if(isIncompleFound) {
                chapter.isLocked = YES;
                continue;
            }
            chapter.isLocked = NO;
            isIncompleFound = !chapter.isCompleted;
        }
        Chapter *chapter = self.course.chapters[0];
        self.textToSpeak = chapter.title;
        [self speak: nil];
    }
    [self.tableView reloadData];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.course.chapters.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ChapterListingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChapterListingCell"];
    Chapter *chapter = self.course.chapters[indexPath.row];
    [cell populateWithChapter: chapter];
    if(chapter.isLocked) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView  heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Chapter *chapter = self.course.chapters[indexPath.row];
    if(!chapter.isLocked) {
        [self performSegueWithIdentifier:@"ChapterDetails" sender:chapter];
    }
}

- (NSString*)screenName {
    return @"Chapter";
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqual: @"ChapterDetails"]){
        Chapter* chapter = (Chapter*) sender;
        ChapterDetailsViewController* vc = (ChapterDetailsViewController*)segue.destinationViewController;
        vc.chapter = chapter;
        vc.course = self.course;
        vc.mediaFileURL = self.course.mediaFileURL;
    }
}


@end

//
//  MorfBaseController.h
//  Morf
//
//  Created by Tushar on 06/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MorfBaseController : UIViewController

- (NSString *)screenName;
- (void)backTapped;
-(void) logout;

@end

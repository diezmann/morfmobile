//
//  MorfTabBarController.m
//  Morf
//
//  Created by Tushar on 01/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "MorfTabBarController.h"
#import "MorfNavigationController.h"

@interface MorfTabBarController ()<UITabBarControllerDelegate>

@end

@implementation MorfTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    [self setupTabBarAppearance];
    self.viewControllers = [self controllers];
    [self setSelectedIndex:self.defaultSelectedIndex];
}

- (void)setupTabBarAppearance {
    UIImage* tabBarBackground = [UIImage imageNamed:@"TabBar_BG"];
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
    [[UITabBarItem appearance] setTitlePositionAdjustment: UIOffsetMake(0.0, -2.0)];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSFontAttributeName :[UIFont fontWithName:@"Georgia" size:10.0f],
                                                         NSForegroundColorAttributeName : [UIColor whiteColor]}
                                             forState:UIControlStateNormal];
    [[UITabBar appearance]setSelectionIndicatorImage:[UIImage imageNamed:@"selection_image"]];
    
}

- (NSArray *)controllers {
    NSArray *tabBarItems = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"TabBarData" ofType:@"plist"]];
    NSMutableArray *controllers = [[NSMutableArray alloc]initWithCapacity:tabBarItems.count];

    int i = 0;
    for(NSDictionary *tabBarItemDetails in tabBarItems) {
        NSString *tabBarItemTitle = [tabBarItemDetails[@"title"] uppercaseString];
        UITabBarItem *tabBarItem = [[UITabBarItem alloc]initWithTitle:tabBarItemTitle
                                                                image:[UIImage imageNamed:tabBarItemDetails[@"iconName"]]
                                                                    tag:i++];
        tabBarItem.image = [tabBarItem.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UIViewController *viewController;
        
        // As there is just one storyboard as of now, making others just as navigation controllers. This will
        // change as more storyboards are added

        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:tabBarItemDetails[@"storyboard"] bundle:[NSBundle mainBundle]];
        UIViewController *rootController = [storyBoard instantiateInitialViewController];
        viewController = [[UINavigationController alloc] initWithRootViewController: rootController];
        viewController.tabBarItem = tabBarItem;
        [controllers addObject:viewController];
    }
    return controllers;
}

-(BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    NSInteger tag = viewController.tabBarItem.tag;
    if(tag == 1 || tag== 4){
        return NO;
    }
    return YES;
}


-(IBAction)logout:(id)sender {
    [self.selectedViewController dismissViewControllerAnimated:YES completion:nil];
}
@end

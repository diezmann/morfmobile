//
//  MorfBaseController.m
//  Morf
//
//  Created by Tushar on 06/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "MorfBaseController.h"
#import "UIBarButtonItem+MorfAdditions.h"
#import "MBProgressHUD.h"
#import "Morf-Swift.h"
#import "AppDelegate.h"

@implementation MorfBaseController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Logo"]];
    UIBarButtonItem *logout = [[UIBarButtonItem alloc] initWithTitle: @"Logout" style:UIBarButtonItemStyleDone target: self action: @selector(logout)];
    logout.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = logout;
}



- (NSString *)screenName {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                            reason:[NSString stringWithFormat:@"You must override %@ in the subclass %@",NSStringFromSelector(_cmd),NSStringFromClass([self class])]
                          userInfo:nil];
    
}

- (void)backTapped {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) logout {
    [[TextToSpeechService sharedInstance] pause];
    MBProgressHUD *loader = [MBProgressHUD showHUDAddedTo: self.navigationController.view animated: YES];
    loader.userInteractionEnabled = NO;
    loader.labelText = @"Logging out";
    loader.mode = MBProgressHUDModeText;
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSArray *allKeys = [[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allKeys];
    for(NSString *key in allKeys) {
        if([key hasPrefix:@"cache_"]) {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey: key];
        }
    }
    [[MorfAPIClient sharedClient] executePostNoDataRequestWithAuthorization: [NSString stringWithFormat:@"%@Account/Logout", [AppDelegate baseUrl]] parameters: @{} success:^(id response) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey: @"accessToken"];
        delegate.isLoggedOut = YES;
        [loader hide: true];
        [self.navigationController.topViewController dismissViewControllerAnimated: YES completion: nil];
        [self.navigationController popToRootViewControllerAnimated: YES];

    } failure:^(NSError *error) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey: @"accessToken"];
        delegate.isLoggedOut = YES;
        [loader hide: true];
            [self.navigationController.topViewController dismissViewControllerAnimated: YES completion: nil];
                        [self.navigationController popToRootViewControllerAnimated: YES];
    }];
}
@end

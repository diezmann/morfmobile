//
//  CourseListingController.m
//  Morf
//
//  Created by Tushar on 02/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "CourseListingController.h"
#import "CourseListingCell.h"
#import "CourseContentService.h"
#import "ChaptersListingController.h"
#import "UIBarButtonItem+MorfAdditions.h"
#import "Course.h"
#import "AppDelegate.h"
#import "Morf-Swift.h"

@interface CourseListingController ()

@property (weak, nonatomic) IBOutlet UILabel *teamScoreLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *courses;
@property (weak, nonatomic) IBOutlet UIView *siriVideoPlayerVIew;
@property (weak, nonatomic) IBOutlet UILabel *userName;

@end

@implementation CourseListingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem morfLeftBarButtonWithTitle:@"Home" image:@"Down_Btn" target:self action:@selector(backToHome)];
    User *user = [AppDelegate currentUser];
    self.userName.text = user.teamName;

    [self.siriVideoPlayerVIew addSubview:self.mediaPlayerView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear: animated];
}

-(void)viewDidAppear:(BOOL)animated {
    User *user = [AppDelegate currentUser];
    self.teamScoreLabel.text= [NSString stringWithFormat:@" %ld Points", user.teamPoints];
}

- (void)backToHome {
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.courses.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CourseListingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CourseListingCell"];
    
    [cell populateWithCourse:self.courses[indexPath.row]];
    if(indexPath.row == [[CourseContentService sharedInstance] numberOfCoursesCompleted]) {
        [cell flash];
    }
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView  heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65.0f;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Course *selectedCourse = self.courses[indexPath.row];
    MBProgressHUD *loader = [MBProgressHUD showHUDAddedTo: self.view animated: YES];
    loader.userInteractionEnabled = NO;
    if(selectedCourse.chapters.count == 0) {
        [loader hide: YES];
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"No chapters available for now" delegate: nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        return;
    }
    [[MorfAPIClient sharedClient] executeRequestWithAuthorization:[NSString stringWithFormat: @"%@Users/%@/CourseOverviews/%@", [AppDelegate baseUrl], [AppDelegate currentUser].id ,selectedCourse.courseId] success:^(id responseObject) {
        [selectedCourse updateOverview: responseObject[@"TopicStats"]];
        [self performSegueWithIdentifier:@"Chapters" sender:self.courses[indexPath.row]];
        [loader hide: YES];
    } failure:^(NSError *error) {
        [loader hide: YES];
        NSString * errorDescription = error.userInfo[@"NSLocalizedDescription"];
        if([[errorDescription lowercaseString] containsString: [@"Request failed: unauthorized" lowercaseString]]) {
            [self logout];
            return;
        }
        [self performSegueWithIdentifier:@"Chapters" sender:self.courses[indexPath.row]];
    }];
}


#pragma mark - getters  

- (NSArray *)courses {
    if(!_courses) {
        _courses = [[CourseContentService sharedInstance] courses];
    }
    return [_courses sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [(Course*)a name];
        NSString *second = [(Course*)b name];
        return [first compare:second];
    }];;
}

#pragma mark - others

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqual:@"Chapters"]) {
        ChaptersListingController *chaptersListingController = (ChaptersListingController *)segue.destinationViewController;
        chaptersListingController.course = (Course *)sender;
    }
}

- (NSString *)screenName {
    return @"Courses";
}

@end

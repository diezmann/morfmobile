
import UIKit

class ChapterDetailsViewController: BaseTTSViewController, UIWebViewDelegate {

    @IBOutlet weak var siriView: UIView!
    @IBOutlet weak var takeQuizButton: UIButton!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var scoreLabel: UILabel!
    var quizAvailable: Bool = false
    
    var chapter: Chapter!
    var course: Course!
    var mediaFileURL: String!
    let ttsService = TextToSpeechService.sharedInstance()
    
    func removeTags(extractedText: String) -> String {
        let r = Range<String.Index>(start: extractedText.startIndex, end: extractedText.endIndex)
        return extractedText.stringByReplacingOccurrencesOfString("_TITLE_", withString: "", options: NSStringCompareOptions.CaseInsensitiveSearch, range: r)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let loader = MBProgressHUD.showHUDAddedTo(self.webView, animated: true)
        loader.userInteractionEnabled = false
        loader.labelText = "Loading content..."
        MorfAPIClient.sharedClient().executeRequestWithAuthorization(chapter.url, success: { (response) -> Void in
            loader.hide(true)
            let dict = response as! Dictionary<String, AnyObject>
            let htmlString = dict["Exposition"] as! String
            self.quizAvailable = dict["Quiz"] as! Bool
            if(!self.quizAvailable) {
                self.takeQuizButton.titleLabel?.text = "NO EXAM"
            }
            self.webView.delegate = self
            self.webView.loadHTMLString("<body>" + self.removeTags(htmlString) + "</body>", baseURL: nil)
            NSUserDefaults.standardUserDefaults().setValue(response, forKey: "Chapter_\(self.chapter.url)")
        }) { (error) -> Void in
            loader.hide(true)
            if let errorDescription = (error.userInfo["NSLocalizedDescription"]) {
                if(errorDescription.lowercaseString.containsString("Request failed: unauthorized".lowercaseString)) {
                self.logout();
                return;
                }
            }
            var message = "Chapter details not availble!"
            if(error.code == -1009) {
                message = "Please check Internet Connection."
            }
            UIAlertView(title: "Error", message: message, delegate: nil, cancelButtonTitle: "Ok").show()

        }
        
        siriView.addSubview(mediaPlayerView!)
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        if let extractedText = self.webView.stringByEvaluatingJavaScriptFromString("document.body.innerText;") {
            textToSpeak = removeTags(extractedText)
            speak(nil)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        scoreLabel.text = "\(AppDelegate.currentUser().teamName) - \(AppDelegate.currentUser().teamPoints) Points"
        if(textToSpeak != nil) {
            ttsService.speak(textToSpeak)
        }
    }
        
    override func screenName() -> String! {
        return "Chapter"
    }
    
    @IBAction func quizTapped(sender: UIButton) {
        if(sender.titleLabel?.text == "NO EXAM") {
            return
        }
        let loader = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loader.userInteractionEnabled = false
        loader.labelText = "Loading Quiz..."
        MorfAPIClient.sharedClient().executeRequestWithAuthorization(chapter.url+"/Questions?secret=89U3q1wbvz3805fWlFHVulCEriPR4qF5fUAQmfAl", success: { (response) -> Void in
            MorfAPIClient.sharedClient().executeRequestWithAuthorization("\(AppDelegate.baseUrl())Teams?includeStats=true&includeMembers=false", success: { (teamResponse) -> Void in
                loader.hide(true)
                let quiz = Quiz(quizArray: response as! NSArray)
                self.chapter.quiz = quiz
                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Quiz") as! QuizViewController
                vc.chapter = self.chapter
                vc.course = self.course
                vc.quiz = quiz
                let teams = teamResponse as! Array<Dictionary<String, AnyObject>>
                vc.highestScoringTeam = teams[0] as Dictionary<String, AnyObject>
                if (quiz.questions.count > 0){
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    loader.hide(true)
                    UIAlertView(title: "Error", message: "Quiz not availble!", delegate: nil, cancelButtonTitle: "Ok").show()
                }

            }) { (error) -> Void in
                loader.hide(true)
                if let errorDescription = (error.userInfo["NSLocalizedDescription"]) {
                    if(errorDescription.lowercaseString.containsString("Request failed: unauthorized".lowercaseString)) {
                        
                        self.logout();
                        return;
                    }
                }
                var message = "Quiz not availble!"
                if(error.code == -1009) {
                    message = "Please check Internet Connection."
                }
                
                UIAlertView(title: "Error", message: message, delegate: nil, cancelButtonTitle: "Ok").show()
            }
            }) { (error) -> Void in
                loader.hide(true)
                if let errorDescription = (error.userInfo["NSLocalizedDescription"]) {
                    if(errorDescription.lowercaseString.containsString("Request failed: unauthorized".lowercaseString)) {

                    self.logout();
                    return;
                    }
                }
                var message = "Quiz not availble!"
                if(error.code == -1009) {
                    message = "Please check Internet Connection."
                }

                UIAlertView(title: "Error", message: message, delegate: nil, cancelButtonTitle: "Ok").show()
        }
    }

}

//
//  CourseListingController.h
//  Morf
//
//  Created by Tushar on 02/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTTSViewController.h"

@interface CourseListingController : BaseTTSViewController <UITableViewDataSource, UITableViewDelegate>

@end

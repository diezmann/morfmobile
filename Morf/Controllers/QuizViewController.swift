
import UIKit

let QuestionHeaderIndex = 0
let QuestionTitleIndex = 1
let ChoiceA = ["index": 0, "tableIndex" : 3, "name" : "A", "audio" : "optiona.aiff"]
let ChoiceB = ["index": 1, "tableIndex" : 5, "name" : "B", "audio" : "optionb.aiff"]
let ChoiceC = ["index": 2, "tableIndex" : 7, "name" : "C", "audio" : "optionc.aiff"]
let ChoiceD = ["index": 3, "tableIndex" : 9, "name" : "D", "audio" : "optiond.aiff"]

class QuizViewController: BaseTTSViewController, UITableViewDataSource, UITableViewDelegate, TextToSpeechServiceDelegate, VoiceToTextServiceDelegate,QuestionScoreDelegate {

    @IBOutlet weak var questionNumberView: QuestionNumberView!
    @IBOutlet weak var timerView: QuizTimerView!
    @IBOutlet weak var quizTableView: UITableView!
    var siriPlayerView: UIView! = nil
    @IBOutlet weak var totalPointsLabel: UILabel!
    @IBOutlet weak var currentQuestionPointsLabel: UILabel!
    @IBOutlet weak var bonusPointsLabel: UILabel!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var teamPoint: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var chapter: Chapter!
    var course: Course!
    var quiz: Quiz!
    private var playBackQueue: Array<String> = []
    private var timer: NSTimer?
    private var lastSelectedChoice: QuizOptionCell?
    private var questionChangeTimerHandler: TimerHandler?
    private var quizCompletionTimerHandler: TimerHandler?
    var highestScoringTeam: Dictionary<String, AnyObject> = Dictionary()
    var cells: Array<QuizCellData> = []
    
    //MARK: - Superclass methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        quiz.timeDelegate = timerView
        quiz.startQuiz()
        userName.text = AppDelegate.currentUser().fullName()
        updateCells()
        updateStatusOfQuiz()
//        if (isPaused) {defaultPlaybackTimer.invalidate()}
        for i in 0...quiz.currentQuestionIndex! {
            questionNumberView.changeAttemptedQuestionIndex(i-1)
        }
        let totalPoints = AppDelegate.currentUser().score
        totalPointsLabel.text = "\(totalPoints) Points"
        showCurrentQuestionPoints()
        siriPlayerView = UIView(frame: (mediaPlayerView?.frame)!)
        siriPlayerView.addSubview(mediaPlayerView!)
        refreshCellsData()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        AnimationUtility.zoomInOutInfinitely(bonusPointsLabel, zoomScale: 1.1, duration: 0.7)
    }
    
    override func viewDidAppear(animated: Bool) {
        let teamName = highestScoringTeam["Name"] as! String
        let points = highestScoringTeam["Points"] as! Int
        teamPoint.text = "\(teamName) with \(points) Points"
        TextToSpeechService.sharedInstance().pause()
        self.refreshCellsData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        VoiceToTextService.sharedInstance().stopListening()
        super.viewWillDisappear(animated)
        timer?.invalidate()
    }
    
    override func screenName() -> String! {
        return "Home"
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        return identifier == "OnlineTutor" && quiz.name == "Quiz_Sexual_Harassment_1"
    }
    
    //MARK: - TableView methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellData = cells[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier(cellData.reuseIdentifier!) as UITableViewCell?
        cellData.configBlock?(cell!)
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.row == QuestionTitleIndex) {
            let constraintSize = CGSizeMake(268/2, CGFloat.max)
            let titleCell = cells[indexPath.row] as! QuestionTitleCell
            if let font = UIFont(name: "Helvetica", size: 17) {
                let attributes = [NSFontAttributeName: font]
                let newSize = (titleCell.titleText as NSString).boundingRectWithSize(constraintSize, options:NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attributes , context: nil)
                return ceil(newSize.height) + 20
            }
        }
        return cells[indexPath.row].height
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! QuizOptionCell
        handleChoice(cell)
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return siriPlayerView;
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return siriPlayerView.frame.size.height;
    }
    //MARK: - Media methods
    
    private func handleSpeechRecognized(recognizedWord: String) {
        let chosenOption = recognizedWord.substringFromIndex(recognizedWord.startIndex.advancedBy(recognizedWord.characters.count-1))
        var indexPath = 0
        for choice in [ChoiceA, ChoiceB, ChoiceC, ChoiceD]{
            if choice["name"] == chosenOption {
                indexPath = choice["tableIndex"] as! Int
                break
            }
        }
        let cell = quizTableView.cellForRowAtIndexPath(NSIndexPath(forRow: indexPath, inSection: 0)) as! QuizOptionCell
        handleChoice(cell)
    }

    //MARK: - Choice handling methods
    
    func refreshCellsData() {
        updateCells()
        if(quiz.isCompleted) {
            return
        }
        quiz.currentQuestion!.scoreDelegate = self
        let questionHeaderCell = cells[QuestionHeaderIndex] as! QuizHeaderCell
        questionHeaderCell.configBlock = {(cell) in
            (cell as! QuizHeaderCell).configureWith(self.quiz.currentQuestionIndex!+1, remainingQuestions: self.quiz.questions.count-self.quiz.currentQuestionIndex!)
        }
        
        let questionTitleCell = cells[QuestionTitleIndex] as! QuestionTitleCell
        questionTitleCell.titleText = quiz.currentQuestion!.question.title
        questionTitleCell.configBlock = {(cell) in
            (cell as! QuestionTitleCell).configureWith(self.quiz.currentQuestion!.question.title)
        }
        
        var textToSpeak = "\(quiz.currentQuestion!.question.title)\n\n\n\n\n\n\n\t"
        for choice in [ChoiceA, ChoiceB, ChoiceC, ChoiceD]{
            if (choice["index"] as! Int > quiz.currentQuestion!.options.count - 1){continue}
            let option = quiz.currentQuestion!.options[choice["index"] as! Int]
            let choiceName = choice["name"] as! String
            textToSpeak = "\(textToSpeak) Option \(choiceName).\n\n\n\n\n\t \(option.text.title)\n\n\n"
            let choiceCell = cells[choice["tableIndex"] as! Int] as! QuizOptionCell
            choiceCell.configBlock = {(cell) in
                cell.userInteractionEnabled = !self.quiz.isPaused
                (cell as! QuizOptionCell).configureWith(choice["tableIndex"] as! Int, name: choice["name"] as! String, desc:option.text.title, isCorrect: option.isCorrect, choice: option)
            }
        }
        self.textToSpeak = textToSpeak
        self.speak(self.textToSpeak)
    }
    
    private func updateCells() {
        cells = []
        cells.append(QuizHeaderCell(identifier: "Header", height: 30.0))
        cells.append(QuestionTitleCell(identifier: "Question", height: 90.0))
        if(quiz.isCompleted) {
            return
        }
        let currentQuestion = quiz.currentQuestion!
        if(currentQuestion.options.count == 0) {
            return
        }
        for _ in 0...currentQuestion.options.count-1 {
            cells.append(QuizCellData(identifier: "Separator", height: 5.0))
            cells.append(QuizOptionCell(identifier: "Answer", height: 40.0))
        }
    }
    
    private func handleChoice(cell: QuizOptionCell){
        stopSiriAndEmptyQueue()
        TextToSpeechService.sharedInstance().pause()
        cell.userInteractionEnabled = false
        lastSelectedChoice?.reset()
        lastSelectedChoice = cell
        if(cell.isRightChoice()){
            cell.markCorrect()
            quiz.currentQuestion!.selectedOption = cell.choice
            changeOptionSelection(false)
            changeQuestion()
        } else {
            cell.markWrong()
            quiz.currentQuestion!.questionAnsweredWrong()
            playFile("Wrong_Answer.aiff")
        }
    }
    
    private func stopSiriAndEmptyQueue() {
        playBackQueue = []
    }
    
    private func changeQuestion() {
        quiz.currentQuestion!.questionEnded()
        updatePoints()
        if (quiz.isLastQuestion) {
            playFile("LastQuestion_RightAnswer.aiff")
            quiz.endQuiz()
            quizCompletionTimerHandler = TimerHandler(timer: NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("handleQuizCompletion"), userInfo: nil, repeats: false))
        } else {
            quiz.pause()
            playFile("Correct_Answer.aiff")
            questionChangeTimerHandler = TimerHandler(timer: NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: Selector("showNewQuestion"), userInfo: nil, repeats: false))
        }
    }
    
    private func updatePoints() {
        let totalPoints = quiz.points + AppDelegate.currentUser().score
        totalPointsLabel.text = "\(totalPoints) Points"
    }

    func showNewQuestion() {
        quiz.resume()
        quiz.nextQuestion()
        refreshCellsData()   //Commenting to avoid double sounds (already called in didFinishPlaying). Need to fix
        quizTableView.reloadData()
        self.tableView.setNeedsLayout()
        //startPlaying()
                //TextToSpeechService.sharedInstance().speak(currentQuestion.question.title)
        showCurrentQuestionPoints()
    }
    
    func showCurrentQuestionPoints() {
        currentQuestionPointsLabel.text = String(quiz.currentQuestion!.score)
        AnimationUtility.zoomAndBounce(currentQuestionPointsLabel, firstZoomScale: 3, firstZoomDelay: 0.1, firstCompletion: { () -> Void in
            }, secondZoomScale: 1.0, secondZoomDelay: 1.6, secondCompletion: nil)
    }

    func transitionToCompletion(progress: MBProgressHUD) {
        progress.hide(true)
        chapter.isCompleted = true
        if(course.completion == 100.0){
            let courseCompletionVC = self.storyboard?.instantiateViewControllerWithIdentifier("CourseCompletion") as! CourseCompletionViewController
            courseCompletionVC.course = course
            courseCompletionVC.chapter = chapter
            self.navigationController?.pushViewController(courseCompletionVC, animated: true)
            return
        }
        self.tabBarController?.selectedIndex = 2
        let navC = self.tabBarController!.viewControllers![2] as! UINavigationController
        let profileVC = navC.viewControllers[0] as! ProfileViewController
        profileVC.playSound()
        self.navigationController?.popToRootViewControllerAnimated(false)
    }

    func doneLogin(responseObject: Dictionary<String, AnyObject>!, image: UIImage, userDetails: Dictionary<String, AnyObject>, overviews: Array<Dictionary<String, AnyObject>>!, team: Dictionary<String, AnyObject>, response: AnyObject, progress: MBProgressHUD, teams: Array<Dictionary<String, AnyObject>>) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let activities = response as! Array<Dictionary<String, AnyObject>>
        let userId = NSUserDefaults.standardUserDefaults().stringForKey("userId")
        let userActivities = activities.filter({$0["UserId"] as? String == userId})
        appDelegate.user = User(response: responseObject, downloadedProfileImage: image, userDetails: userDetails, overviews: overviews, team: team)
        appDelegate.user.activities = userActivities.map({$0["DisplayText"] as! String})
        var rank:Int = 0;
        for team in teams {
            rank++
            if(team["Id"] as! Int == AppDelegate.currentUser().teamId) {
                break
            }
        }
        appDelegate.user.teamRank = rank
        self.transitionToCompletion(progress)
    }

    func makeAllCalls(progress: MBProgressHUD) {
        let username = NSUserDefaults.standardUserDefaults().valueForKey("userName") as! String
        let password = NSUserDefaults.standardUserDefaults().valueForKey("password") as! String
        let url = "\(AppDelegate.baseUrl())Account/Login/\(username)/\(password)"
        MorfAPIClient().executeRequest(url, success: { (responseObject) -> Void in
            let user = responseObject as! NSDictionary
            let accessToken = user.valueForKey("access_token") as! String
            let userId = user.valueForKey("Id") as! String
            NSUserDefaults.standardUserDefaults().setValue(accessToken, forKey: "accessToken")
            NSUserDefaults.standardUserDefaults().setValue(userId, forKey: "userId")
            NSUserDefaults.standardUserDefaults().setValue(username, forKey: "userName")
            NSUserDefaults.standardUserDefaults().setValue(password, forKey: "password")
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.isLoggedOut = false
            MorfAPIClient().executeRequestWithAuthorization("\(AppDelegate.baseUrl())/Users/\(userId)", success: { (userDetails: AnyObject) -> Void in
                MorfAPIClient().executeRequestWithAuthorization("\(AppDelegate.baseUrl())/Users/\(userId)/CourseOverviews/\(self.course.courseId)", success: { (courseByCourseIdResponse) -> Void in
                MorfAPIClient().executeRequestWithAuthorization("\(AppDelegate.baseUrl())/Users/\(userId)/CourseOverviews", success: { (response) -> Void in
                    let overview = response as! NSArray
                    MorfAPIClient().executeRequestWithAuthorization("\(AppDelegate.baseUrl())/Users/\(userId)/Team", success: { (teamResponse) -> Void in
                        ImageLoader().imageForUrl("\(AppDelegate.hostUrl())WebApp/ProfilePictures/\(userId).png",
                            completionHandler: { (image, url) -> () in
                                MorfAPIClient().executeRequestWithAuthorization("\(AppDelegate.baseUrl())Users/\(userId)/RecentActivity?limit=8", success: { (response: AnyObject) -> Void in
                                    MorfAPIClient().executeRequestWithAuthorization("\(AppDelegate.baseUrl())Teams?includeStats=true&includeMembers=false", success: { (teamsResponse) -> Void in
                                        self.doneLogin(responseObject as! Dictionary<String, AnyObject>, image: image!, userDetails: userDetails as! Dictionary<String, AnyObject>, overviews: overview as! Array<Dictionary<String, AnyObject>>, team: teamResponse as! Dictionary<String, AnyObject>, response: response, progress: progress, teams: teamsResponse as! Array<Dictionary<String, AnyObject>>)
                                        
                                        }, failure: { (error) -> Void in
                                            self.doneLogin(responseObject as! Dictionary<String, AnyObject>, image: image!, userDetails: userDetails as! Dictionary<String, AnyObject>, overviews: overview as! Array<Dictionary<String, AnyObject>>, team: teamResponse as! Dictionary<String, AnyObject>, response: [], progress: progress, teams: Array())
                                    })
                                    }) { (error: NSError) -> Void in
                                    self.transitionToCompletion(progress)
                                }
                        })
                        }, failure: { (error) -> Void in
                        self.transitionToCompletion(progress)
                    })
                    }, failure: { (error) -> Void in
                        self.transitionToCompletion(progress)
                })
                    }, failure: { (error) -> Void in
                        self.transitionToCompletion(progress)
                })

                }, failure: { (error: NSError) -> Void in
                    self.transitionToCompletion(progress)
            })
            }) { (error) -> Void in
                self.transitionToCompletion(progress)
        }
    }

    
    func handleQuizCompletion() {
        self.chapter.isCompleted = true
        AppDelegate.currentUser().score += quiz.points
        var answers : Array<Dictionary<String, AnyObject>> = []
        for question in quiz.questions {
            answers.append(question.parametarize())
        }
        let parameters = ["CourseId": course.courseId, "TopicId": chapter.topicId, "StartTime": quiz.startTime!.description, "EndTime": quiz.endTime!.description, "Answers" : answers]
        let progress = MBProgressHUD.showHUDAddedTo(self.navigationController?.view, animated: true)
        progress.userInteractionEnabled = false
        progress.labelText = "Submitting"

        MorfAPIClient.sharedClient().executePostRequestWithAuthorization("\(AppDelegate.baseUrl())QuizAttempt/Record?secret=89U3q1wbvz3805fWlFHVulCEriPR4qF5fUAQmfAl", parameters: parameters as! Dictionary<String, AnyObject>, success: { (response: AnyObject) -> Void in
            self.makeAllCalls(progress)
            }) { (error: NSError) -> Void in
            self.makeAllCalls(progress)
        }
    }

    @IBAction func pauseQuizTapped(sender: UIButton) {
        quiz.togglePause()
        if(quiz.isPaused) {
            TextToSpeechService.sharedInstance().pause()
            VoiceToTextService.sharedInstance().stopListening()
        } else {
            VoiceToTextService.sharedInstance().startListening(self)
        }
//        var mediaFunc = isPaused == true ? stopPlaying : startPlaying
        updateStatusOfQuiz()
        
        if (quiz.isPaused) {pauseTimers()} else {resumeTimers()}
    }
    
    private func updateStatusOfQuiz() {
//        if (isPaused) {mediaPlayerView.pauseGame()}
//        else {mediaPlayerView.resumeGame()}
        let image = quiz.isPaused ? UIImage(named: "Player_Play") : UIImage(named: "Player_Pause")
        playPauseButton.setImage(image, forState: .Normal)
        changeOptionSelection(!quiz.isPaused)
    }
    
    private func changeOptionSelection(predicate: Bool) {
        for i in 0...cells.count-1 {
            let cell = quizTableView.cellForRowAtIndexPath(NSIndexPath(forRow: i, inSection: 0))
            cell?.userInteractionEnabled = predicate && cell!.isKindOfClass(QuizOptionCell)
        }
    }
    
    private func pauseTimers() {
        questionChangeTimerHandler?.pause()
        quizCompletionTimerHandler?.pause()
    }
    
    private func resumeTimers() {
        questionChangeTimerHandler?.resume()
        quizCompletionTimerHandler?.resume()
    }
    
    func didFinishSpeaking() {
        VoiceToTextService.sharedInstance().startListening(self)
    }
    
    func didReceiveSpeechInput(input: String) {
        if(input == "Pause" && !quiz.isPaused) {
            pauseQuizTapped(playPauseButton)
            return
        }
        if(input == "Resume" && quiz.isPaused) {
            pauseQuizTapped(playPauseButton)
            return
        }
        if(input == "Stop" && quiz.isPaused) {
            self.dismissViewControllerAnimated(true, completion: nil)
            return
        }
        for choice in [ChoiceA, ChoiceB, ChoiceC, ChoiceD] {
            if(input == (choice["name"] as! String)) {
                let indexPath = choice["tableIndex"] as! Int
                handleChoice(quizTableView.cellForRowAtIndexPath(NSIndexPath(forRow: indexPath, inSection: 0)) as! QuizOptionCell)
                return
            }
        }
    }
    
    func onScoreUpdated(score : Int) {
        self.currentQuestionPointsLabel.text = String(score)
    }
    
    func updateProgress(progress: Float) {
        
    }
}

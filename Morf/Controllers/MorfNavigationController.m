//
//  MorfNavigationController.m
//  Morf
//
//  Created by Tushar on 06/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "MorfNavigationController.h"
#import "MorfBaseController.h"
#import "UIBarButtonItem+MorfAdditions.h"

@implementation MorfNavigationController

#pragma mark - Nav Bar Delegate methods 


- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    if(self.topViewController) {
        MorfBaseController *previousViewController = (MorfBaseController *)self.topViewController;
        viewController.navigationItem.leftBarButtonItem = [UIBarButtonItem morfBackBarButtonWithTitle:[previousViewController screenName]
                                                                                               target:viewController
                                                                                               action:@selector(backTapped)];
        
    }
    
    [super pushViewController:viewController animated:animated];
}

@end

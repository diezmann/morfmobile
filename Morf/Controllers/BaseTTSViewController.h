//
//  BaseTTSViewController.h
//  
//
//  Created by Senthil Kumar on 9/22/15.
//
//

#import "MorfBaseController.h"
@class TTSView;

@interface BaseTTSViewController : MorfBaseController

@property (nonatomic, strong) NSString *textToSpeak;
@property (nonatomic, weak) IBOutlet TTSView *mediaPlayerView;

- (IBAction) speak: (id)sender;
- (void) playFile: (NSString *) fileToPlay;

@end

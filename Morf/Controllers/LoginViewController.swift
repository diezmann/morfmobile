
import UIKit

class LoginViewController: UIViewController, LoginDelegate
{

    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var backgroundImageView: UIScrollView!
    @IBOutlet weak var companyCodeField: UITextField!
    
//    func doneLogin(responseObject: Dictionary<String, AnyObject>!, image: UIImage, userDetails: Dictionary<String, AnyObject>, overviews: Array<Dictionary<String, AnyObject>>!, team: Dictionary<String, AnyObject>, response: AnyObject, progress: MBProgressHUD) {
//        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        let activities = response as! Array<Dictionary<String, AnyObject>>
//        let userId = NSUserDefaults.standardUserDefaults().stringForKey("userId")
//        let userActivities = activities.filter({$0["UserId"] as? String == userId})
//        appDelegate.user = User(response: responseObject, downloadedProfileImage: image, userDetails: userDetails, overviews: overviews, team: team)
//        appDelegate.user.activities = userActivities.map({$0["DisplayText"] as! String})
//        progress.hide(true)
//        self.goToHomePage()
//    }
    
    @IBAction func loginTapped(sender: UIButton)
    {
        if let username = userNameField.text, password = passwordField.text, companyCode = companyCodeField.text
        {
            if(username.isEmpty || password.isEmpty || companyCode.isEmpty)
            {
                UIAlertView(title: "Error", message: "Enter a valid username/password/company code", delegate: nil, cancelButtonTitle: "Ok").show()
                return
            }
            NSUserDefaults.standardUserDefaults().setValue(companyCode, forKey: "companyCode")
            LoginService.sharedInstance.delegate = self
            LoginService.sharedInstance.login(username, password: password, companyName: companyCode,delegate: self)
        }
       
//        let manager = AFHTTPRequestOperationManager()
//        manager.responseSerializer = AFJSONResponseSerializer()
//        let url = "\(AppDelegate.baseUrl())Account/Login/\(username)/\(password)"
//        
//        let progress = MBProgressHUD.showHUDAddedTo(self.navigationController?.view, animated: true)
//        progress.userInteractionEnabled = false
//        progress.labelText = "Logging In..."
//        MorfAPIClient().executeRequest(url, success: { (responseObject) -> Void in
//            let user = responseObject as! NSDictionary
//            let accessToken = user.valueForKey("access_token") as! String
//            let userId = user.valueForKey("Id") as! String
//            NSUserDefaults.standardUserDefaults().setValue(accessToken, forKey: "accessToken")
//            NSUserDefaults.standardUserDefaults().setValue(userId, forKey: "userId")
//            NSUserDefaults.standardUserDefaults().setValue(username, forKey: "userName")
//            NSUserDefaults.standardUserDefaults().setValue(password, forKey: "password")
//            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//            appDelegate.isLoggedOut = false
//            MorfAPIClient().executeRequestWithAuthorization("\(AppDelegate.baseUrl())/Users/\(userId)", success: { (userDetails: AnyObject) -> Void in
//                    MorfAPIClient().executeRequestWithAuthorization("\(AppDelegate.baseUrl())/Users/\(userId)/CourseOverviews", success: { (response) -> Void in
//                        let overview = response as! NSArray
//                        MorfAPIClient().executeRequestWithAuthorization("\(AppDelegate.baseUrl())/Users/\(userId)/Team", success: { (teamResponse) -> Void in
//                                ImageLoader().imageForUrl("\(AppDelegate.hostUrl())WebApp/ProfilePictures/\(userId).png",
//                                    completionHandler: { (image, url) -> () in
//                                        MorfAPIClient().executeRequestWithAuthorization("\(AppDelegate.baseUrl())Users/\(userId)/RecentActivity?limit=8", success: { (response: AnyObject) -> Void in
//                                                self.doneLogin(responseObject as! Dictionary<String, AnyObject>, image: image!, userDetails: userDetails as! Dictionary<String, AnyObject>, overviews: overview as! Array<Dictionary<String, AnyObject>>, team: teamResponse as! Dictionary<String, AnyObject>, response: response, progress: progress)
//                                            }) { (error: NSError) -> Void in
//                                                self.doneLogin(responseObject as! Dictionary<String, AnyObject>, image: image!, userDetails: userDetails as! Dictionary<String, AnyObject>, overviews: overview as! Array<Dictionary<String, AnyObject>>, team: teamResponse as! Dictionary<String, AnyObject>, response: [], progress: progress)
//                                        }
//                                })
//                            }, failure: { (error) -> Void in
//                            self.handleFailure(progress, error: error)
//                        })
//                    }, failure: { (error) -> Void in
//                        self.handleFailure(progress, error: error)
//                    })
//                }, failure: { (error: NSError) -> Void in
//                self.handleFailure(progress, error: error)
//            })
//        }) { (error) -> Void in
//            self.handleFailure(progress, error: error)
//        }
//        }
    }
    
//    func handleFailure(progress: MBProgressHUD, error: NSError) {
//        progress.hide(true);
//        var message = "Invalid username, password and/or company code !"
//        if(error.code == -1009) {
//            message = "Please check Internet Connection."
//        }
//        UIAlertView(title: "Login Failed", message: message, delegate: nil, cancelButtonTitle: "Ok").show()
//        NSLog("%@", error)
//    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.passwordField.text = ""
        if let _ = NSUserDefaults.standardUserDefaults().valueForKey("accessToken") {
            self.userNameField.text = NSUserDefaults.standardUserDefaults().valueForKey("userName") as? String
            self.passwordField.text = NSUserDefaults.standardUserDefaults().valueForKey("password") as? String
            self.companyCodeField.text = NSUserDefaults.standardUserDefaults().valueForKey("companyCode") as? String
            loginTapped(UIButton())
        }
    }
    
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImageView.backgroundColor = UIColor(patternImage: UIImage(named: "Login_Background")!);
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: Selector("handleTap:")))
    }
    
    func handleTap(sender: AnyObject) {
        userNameField.resignFirstResponder()
        passwordField.resignFirstResponder()
        companyCodeField.resignFirstResponder()
    }
    
    private func goToHomePage() {
        let homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as UIViewController?
        self.navigationController?.pushViewController(homeVC!, animated: true)
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if(self.view.frame.size.height != self.view.superview?.frame.size.height) {
            return
        }
        backgroundImageView.frame = CGRectMake(backgroundImageView.frame.origin.x, backgroundImageView.frame.origin.y, backgroundImageView.frame.size.width, backgroundImageView.frame.size.height - 215 + 50)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if(self.view.frame.size.height == self.view.superview?.frame.size.height) {
            return
        }
        backgroundImageView.frame = CGRectMake(backgroundImageView.frame.origin.x, backgroundImageView.frame.origin.y, backgroundImageView.frame.size.width, backgroundImageView.frame.size.height + 215 - 50)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false
    }
    
    func loginFailedWithError(error:NSError)
    {
//        var message = "Invalid username, password and/or company code !"
//        if(error.code == -1009)
//        {
//            message = "Please check Internet Connection."
//        }
//        UIAlertView(title: "Login Failed", message: message, delegate: nil, cancelButtonTitle: "Ok").show()
        NSLog("%@", error)
    }
    func loggedInSuccessfully()
    {
        self.goToHomePage()
    }
}

//
//  BaseTTSViewController.m
//  
//
//  Created by Senthil Kumar on 9/22/15.
//
//

#import "BaseTTSViewController.h"
#import "Morf-Swift.h"

@interface BaseTTSViewController ()
@property (nonatomic, strong) MorfMediaPlayer *mediaPlayer;
@end

@implementation BaseTTSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSBundle mainBundle] loadNibNamed:@"MorfTTSView" owner:self options:nil];
}

- (IBAction) speak: (id)sender {
    self.mediaPlayerView.textToSpeak = self.textToSpeak;
    [self.mediaPlayerView play];
}

- (void) playFile: (NSString *) fileToPlay {
    [self.mediaPlayerView playFile: fileToPlay];
}
@end

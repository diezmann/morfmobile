//
//  BaseSiriPlayerController.m
//  Morf
//
//  Created by Tushar on 04/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "BaseMediaPlayerController.h"
#import "Morf-Swift.h"

@interface BaseMediaPlayerController () 

@property (weak, nonatomic) NSTimer *timer;
@property (strong, nonatomic)NSString *playbackfileName;
@property (strong, nonatomic) NSString *textToPlay;
@property (nonatomic) BOOL isPlayingText;

@end

@implementation BaseMediaPlayerController


- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSBundle mainBundle] loadNibNamed:@"MorfMediaPlayerView" owner:self options:nil];
    
    self.mediaPlayerView.delegate = self;
    _defaultPlaybackTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(startPlaying) userInfo:nil repeats:NO];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.mediaPlayer setPlayBackFile:self.playbackfileName];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    [[TextToSpeechService sharedInstance] pause];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.mediaPlayer reset];
    self.mediaPlayer = nil;
    [self.mediaPlayerView resetView];
    [_defaultPlaybackTimer invalidate];
}


- (void)setPlayBackFile:(NSString *)fileName {
    
    self.playbackfileName = fileName;
    [self.mediaPlayer setPlayBackFile:fileName];
}

- (void)setPlayBackFileAndPlay:(NSString *)fileName {
    self.isPlayingText = NO;
    [self setPlayBackFile:fileName];
    [self startPlaying];
}

- (void)playText: (NSString *) text {
    self.isPlayingText = YES;
    self.textToPlay = text;
}
- (void)updateViewWithCurrentPlayBackState {
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.mediaPlayerView setProgressValue:[self.mediaPlayer currentProgressInFraction]];
    }];
    
}

- (void)startPlaying {
    self.mediaPlayerView.isPlaying = YES;
    [self.timer fire];
    if(self.textToPlay) {
        [[TextToSpeechService sharedInstance] speak: self.textToPlay];
    } else {
        [self.mediaPlayer play];
    }
}

- (void)stopPlaying {
    self.mediaPlayerView.isPlaying = NO;
    [self.timer invalidate];
    [self.mediaPlayer pause];
}

#pragma mark - Media View Delegate 

static const NSInteger SEEK_INTERVAL = 2.0;
static const NSInteger PLAYER_VIEW_UPDATE_INTERVAL = 0.5;


- (void)didSeekToProgressValue:(CGFloat)value {
    
    NSTimeInterval timeInterval= CMTimeGetSeconds(self.mediaPlayer.currentItem.duration) * value;
    [self.mediaPlayer seekToTime:CMTimeMake(timeInterval, 1)];
}

- (void)didPlay {
    [self startPlaying];
}

- (void)didPause {
    [self stopPlaying];
}

- (void)didSeekForward {
    
    [self.mediaPlayer moveByTimeInterval:+SEEK_INTERVAL];

}

- (void)didSeekBack {
    
    [self.mediaPlayer moveByTimeInterval:-SEEK_INTERVAL];
}

#pragma mark - Media Player Delegate 

- (void)isReadyToPlay {
    
    [self.mediaPlayerView enableMediaPlayerView];
}

- (void)didFinishPlaying {
    [self.timer invalidate];
    [UIView animateWithDuration:0.5 animations:^{
        [self.mediaPlayerView setProgressValue:1];
    }];

    [self.mediaPlayer seekToTime:kCMTimeZero];
}

#pragma mark - Getters

- (MorfMediaPlayer *)mediaPlayer {
    if(!_mediaPlayer) {
        _mediaPlayer = [[MorfMediaPlayer alloc]init];
        _mediaPlayer.delegate = self;
    }
    return _mediaPlayer;
}



- (NSTimer *)timer {
    if(!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval: PLAYER_VIEW_UPDATE_INTERVAL
                                                           target:self
                                                         selector:@selector(updateViewWithCurrentPlayBackState)
                                                         userInfo:nil
                                                          repeats: YES];
    }
    return _timer;
}



@end

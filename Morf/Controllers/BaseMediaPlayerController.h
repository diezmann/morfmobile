//
//  BaseSiriPlayerController.h
//  Morf
//
//  Created by Tushar on 04/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "MorfBaseController.h"
#import "MorfMediaPlayerView.h"
#import "MorfMediaPlayer.h"

@interface BaseMediaPlayerController : MorfBaseController<MorfMediaPlayerViewDelegate, MorfMediaPlayerDelegate>

@property (nonatomic, strong) IBOutlet MorfMediaPlayerView *mediaPlayerView;
@property (nonatomic) MorfMediaPlayer *mediaPlayer;
@property (nonatomic, strong) NSTimer *defaultPlaybackTimer;

- (void)setPlayBackFile:(NSString *)fileName;
- (void)playText: (NSString *) text;
- (void)setPlayBackFileAndPlay:(NSString *)fileName;
- (void)startPlaying;
- (void)stopPlaying;

@end


import UIKit

class OnlineTutorViewController: UITableViewController {

    @IBAction func doneButtonTapped(sender: AnyObject) {
        presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }

}

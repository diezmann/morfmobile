//
//  VoiceToTextServiceDelegate.swift
//  Morf
//
//  Created by Senthil Kumar on 9/14/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

import Foundation

protocol VoiceToTextServiceDelegate {
    func didReceiveSpeechInput(input: String)
}
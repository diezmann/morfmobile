//
//  TextToSpeechProtocol.swift
//  Morf
//
//  Created by Senthil Kumar on 9/14/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

import Foundation

@objc protocol TextToSpeechServiceDelegate {
    func didFinishSpeaking();
    func updateProgress(progress: Float)
}

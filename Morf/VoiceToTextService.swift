//
//  VTTController.swift
//  Morf
//
//  Created by Senthil Kumar on 9/14/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

import Foundation

class VoiceToTextService : NSObject, OEEventsObserverDelegate, VoiceToTextServiceDelegate {
    static private let service = VoiceToTextService()

    let fliteController = OEFliteController()
    let openEarsEventsObserver = OEEventsObserver()
    let slt = Slt()
    var pathToGeneratedLanguageModel : String = ""
    var pathToGeneratedLanguageDictionary : String = ""
    var usingStartingLanguageModel : Bool = false
    var vttDelegate : VoiceToTextServiceDelegate? = nil
    
    class func sharedInstance() -> VoiceToTextService {
        return service
    }
    
    override init() {
        super.init()
        self.openEarsEventsObserver.delegate = self;
        do {
            try OEPocketsphinxController.sharedInstance().setActive(true)
        } catch _ {
        }
        let languageArray = ["A", "B", "C", "D", "Pause", "Resume", "Stop"]
        let languageModelGenerator = OELanguageModelGenerator()
        languageModelGenerator.verboseLanguageModelGenerator = true;
        let model = OEAcousticModel.pathToModel("AcousticModelEnglish")
        let error = languageModelGenerator.generateLanguageModelFromArray(languageArray, withFilesNamed: "OpenEarsDynamicLanguageModel", forAcousticModelAtPath: model)
        if(error != nil) {
            print("Error in generaing model \(error)")
        } else {
            self.pathToGeneratedLanguageModel = languageModelGenerator.pathToSuccessfullyGeneratedLanguageModelWithRequestedName("OpenEarsDynamicLanguageModel")
            self.pathToGeneratedLanguageDictionary = languageModelGenerator.pathToSuccessfullyGeneratedDictionaryWithRequestedName("OpenEarsDynamicLanguageModel")
        }
        self.usingStartingLanguageModel = true;
    }
    
    func pocketsphinxDidReceiveHypothesis(hypothesis: String, recognitionScore: String, utteranceID: String) {
        print("Local callback: The received hypothesis is \(hypothesis) with a score of \(recognitionScore) and an ID of \(utteranceID)")
        self.vttDelegate?.didReceiveSpeechInput(hypothesis)
        stopListening()
    }
    
    
    func startListening(delegate: VoiceToTextServiceDelegate) {
        self.vttDelegate = delegate
        if(!OEPocketsphinxController.sharedInstance().isListening) {
            OEPocketsphinxController.sharedInstance().startListeningWithLanguageModelAtPath(self.pathToGeneratedLanguageModel, dictionaryAtPath: self.pathToGeneratedLanguageDictionary, acousticModelAtPath: OEAcousticModel.pathToModel("AcousticModelEnglish"), languageModelIsJSGF: false);
        }
    }
    
    func stopListening() {
        OEPocketsphinxController.sharedInstance().stopListening();
    }
    
    func didReceiveSpeechInput(input: String) {
        
    }
}
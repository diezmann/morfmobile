


#import <UIKit/UIKit.h>
#import "BaseMediaPlayerController.h"

@interface TeamMainViewController : BaseMediaPlayerController

@property (weak, nonatomic) IBOutlet UIView *childContainer;
@property (weak, nonatomic) IBOutlet UISegmentedControl *btnSegment;

@end


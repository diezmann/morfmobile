//
//  TeamsViewController.m
//  Team
//
//  Created by Safad Mohd on 6/10/15.
//  Copyright © 2015 Safad Mohd. All rights reserved.
//

#import "TeamsViewController.h"
#import "TeamsTableViewCell.h"
#import "Morf-Swift.h"
#import "TeamContentService.h"
#import "TeamPlayersViewController.h"
#import "Team.h"
@interface TeamsViewController ()<UITableViewDataSource, UITableViewDelegate>{
    NSMutableArray *teams;
}
@end

@implementation TeamsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getTeamDetails];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getTeamDetails{
    self.teamsTableView.hidden = YES;
    MorfAPIClient *client = [MorfAPIClient sharedClient];
    MBProgressHUD *progress = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progress.labelText = @"Loading Team...";
    [client executeRequestWithAuthorization:[NSString stringWithFormat:@"%@/Teams?includeStats=TRUE&includeMembers=FALSE",[AppDelegate baseUrl]] success:^(id response) {
        [progress hide:YES];
        NSLog(@"%@",response);
        self.teamsTableView.hidden = false;
        teams = [TeamContentService getTeams:response];
        self.teamsTableView.hidden = NO;
        [self.teamsTableView reloadData];
    } failure:^(NSError * error) {
        [progress hide: YES];
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"You are offline. Please go online to view updated Team information" delegate: nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        
    }];
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return teams.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"teamCellId";
    TeamsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    Team *team = [teams objectAtIndex:indexPath.row];
    cell.lblTeamRank.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    cell.lblTeamName.text = team.name;
    cell.lblTeamPoint.text = team.points;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Team" bundle:[NSBundle mainBundle]];
    TeamPlayersViewController *controller = (TeamPlayersViewController *) [storyBoard instantiateViewControllerWithIdentifier:@"teamPlayerViewId"];
    controller.team = [teams objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:controller animated:YES];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

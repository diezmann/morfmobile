//
//  TeamsViewController.h
//  Team
//
//  Created by Safad Mohd on 6/10/15.
//  Copyright © 2015 Safad Mohd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeamsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *teamsTableView;

@end

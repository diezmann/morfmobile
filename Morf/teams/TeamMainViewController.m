//
//  ViewController.m
//  Team
//
//  Created by Safad Mohd on 6/10/15.
//  Copyright © 2015 Safad Mohd. All rights reserved.
//

#import "TeamMainViewController.h"
#import "Morf-Swift.h"
@interface TeamMainViewController ()

@end

@implementation TeamMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Teams";
    
    self.btnSegment.layer.cornerRadius = 15.0f;
    self.btnSegment.layer.masksToBounds = YES;
    [self.btnSegment setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:20.0],
                                              NSForegroundColorAttributeName:[UIColor blackColor]}
                                   forState:UIControlStateNormal];
    
    self.btnSegment.backgroundColor = [UIColor whiteColor];
    
    [self.btnSegment addTarget:self
                        action:@selector(segmentSwitch:)
              forControlEvents:UIControlEventValueChanged];
    [self addTeamPlayerView];
   
    
    
    
    
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)addTeamPlayerView{
    [self removeViewFromChildContaner];
    [self addViewToChildContainer:[self getViewController:@"teamPlayerViewId"]];
    
}

-(void)addTeamsView{
    [self removeViewFromChildContaner];
    [self addViewToChildContainer:[self getViewController:@"teamViewId"]];
    
}

-(void)addViewToChildContainer:(UIViewController *)childVc{
    [self addChildViewController:childVc];
    childVc.view.frame = self.childContainer.bounds;
    [self.childContainer addSubview:childVc.view];
    [childVc didMoveToParentViewController:self];
}


-(UIViewController *)getViewController:(NSString *)vcId{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Team" bundle:nil];
    return [storyBoard instantiateViewControllerWithIdentifier:vcId];
}

-(void)removeViewFromChildContaner{
    UIViewController *vc = [self.childViewControllers lastObject];
    [vc willMoveToParentViewController:nil];
    [vc.view removeFromSuperview];
    [vc removeFromParentViewController];
}


- (IBAction)segmentSwitch:(UISegmentedControl *)sender {
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    if (selectedSegment == 0) {
        [self addTeamPlayerView];
    }
    else{
        [self addTeamsView];
    }
}

@end

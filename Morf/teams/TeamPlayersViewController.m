//
//  TeamPlayersViewController.m
//  Team
//
//  Created by Safad Mohd on 6/10/15.
//  Copyright © 2015 Safad Mohd. All rights reserved.
//

#import "TeamPlayersViewController.h"
#import "TeamPlayerTableViewCell.h"
#import  "CircleProgressBar.h"
#import "Morf-Swift.h"
#import "TeamContentService.h"
#import "Team.h"

@interface TeamPlayersViewController (){
    Team *userTeam;
}
@property (weak, nonatomic) IBOutlet CircleProgressBar *circularView;
@property (weak, nonatomic) IBOutlet UIImageView *bckgImageView;

@end

@implementation TeamPlayersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getUserTeamDetails];
    if(self.team){
        self.title = self.team.name;
        self.bckgImageView.hidden=NO;
    }else{
        self.bckgImageView.hidden=YES;

    }
    
    // Do any additional setup after loading the view.
}


-(void)getUserTeamDetails{
    self.teamInfoView.hidden = YES;
    self.teamMembersTableView.hidden= YES;
    MorfAPIClient *client = [MorfAPIClient sharedClient];
    MBProgressHUD *progress = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progress.labelText = @"Loading My Team...";
    
    NSString *url;
    if(self.team){
        url = [NSString stringWithFormat:@"%@/Teams/%@?includeStats=TRUE&includeMembers=TRUE",[AppDelegate baseUrl], self.team.teamId];
    }else{
        NSString *userId = [[NSUserDefaults standardUserDefaults] valueForKey:@"userId"];
        url = [NSString stringWithFormat:@"%@Users/%@/Team",[AppDelegate baseUrl], userId];
    }

    [client executeRequestWithAuthorization:url success:^(id response) {
       [progress hide:YES];
        userTeam = [[Team alloc]initWithInfoDict:response];
        userTeam.members = [TeamContentService sortArray:userTeam.members];
        [self updateUserTeamView];
        NSLog(@"%@",response);
    } failure:^(NSError * error) {
        [progress hide: YES];
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"You are offline. Please go online to view updated Team information" delegate: nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        
    }];
}


-(void)updateUserTeamView{
    self.teamInfoView.hidden=NO;
    self.teamMembersTableView.hidden= NO;
    self.lblTeamName.text = userTeam.name;
    self.lblTeamPoints.text = userTeam.points;
    [self addProgressBarCircleView:userTeam.percentageComplete.floatValue/100.0f];
    [self.teamMembersTableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
}


-(void)addProgressBarCircleView:(CGFloat)progress{
    [self.circularView setProgressBarWidth:10.0f];
    [self.circularView setProgressBarProgressColor:[UIColor whiteColor]];
    
    [self.circularView setProgressBarTrackColor:[UIColor colorWithRed:124/255.0f green:192/255.0f blue:49/255.0f alpha:1.0f]];
    [self.circularView setProgress:progress animated:YES];
    // Hint View Customization
    [self.circularView setStartAngle:270.0f];
    [self.circularView setHintViewSpacing:10.0f];
    [self.circularView setHintViewBackgroundColor:[UIColor clearColor]];
    [self.circularView setHintTextFont:[UIFont fontWithName:@"Helvetica-Bold" size:20.0f]];
    [self.circularView setHintTextColor:[UIColor whiteColor]];
    [self.circularView setHintTextGenerationBlock:^NSString *(CGFloat progress) {
        return [NSString stringWithFormat:@"%.0f%%",progress*100];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return userTeam.members.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"teamPlayerCellId";
    TeamPlayerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    NSDictionary *member =[userTeam.members objectAtIndex:indexPath.row];
    cell.lblteamPlayerRank.text = [NSString stringWithFormat:@"%lu",indexPath.row+1];
    [[ImageLoader sharedLoader] imageForUrl:[NSString stringWithFormat:@"%@/WebApp/ProfilePictures/%@.png",[AppDelegate hostUrl],[member valueForKey:@"Id"]] completionHandler:^(UIImage *image, NSString *str) {
        cell.imgViewTeamPlayer.image= image;
    }];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;

    
    
//    cell.imgViewTeamPlayer.image = [UIImage imageNamed:@"Profile_circle"];
    cell.lblTeamPlayerName.text = [NSString stringWithFormat:@"%@ %@",[member valueForKey:@"FirstName"],[member valueForKey:@"LastName"]];
    cell.lblteamPlayerPoints.text =[NSString stringWithFormat:@"%@ Points",[member valueForKey:@"Points"]];
    return cell;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

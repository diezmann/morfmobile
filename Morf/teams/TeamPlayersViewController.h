//
//  TeamPlayersViewController.h
//  Team
//
//  Created by Safad Mohd on 6/10/15.
//  Copyright © 2015 Safad Mohd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class  Team;

@interface TeamPlayersViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTeamName;
@property (weak, nonatomic) IBOutlet UILabel *lblTeamPoints;
@property (weak, nonatomic) IBOutlet UITableView *teamMembersTableView;
@property (weak, nonatomic) IBOutlet UIView *teamInfoView;
@property (weak, nonatomic) Team *team;

@end


import Foundation

class QuizContentService {
    
    static private let service = QuizContentService()
    var quizes: Array<Quiz> = []
    
    class func sharedInstance() -> QuizContentService {
        return service
    }
    
    func initialize() {
        quizes.append(createQuizFor("Quiz_Sexual_Harassment", index: 1))
        quizes.append(createQuizFor("Quiz_Money_Laundering", index: 1))
        quizes.append(createQuizFor("Quiz_Money_Laundering", index: 2))
        quizes.append(createQuizFor("Quiz_Money_Laundering", index: 3))
        quizes.append(createQuizFor("Quiz_Road_Safety", index: 1))
        quizes.append(createQuizFor("Quiz_Road_Safety", index: 2))
        quizes.append(createQuizFor("Quiz_Road_Safety", index: 3))
    }
    
    func quizFor(name: String) -> Quiz {
        return self.quizes.filter({ (quiz) -> Bool in
            return quiz.name == name
        }).first!
    }
    
    private func createQuizFor(name: String, index: Int) -> Quiz {
        let questionsArray = NSArray(contentsOfFile: NSBundle.mainBundle().pathForResource(name, ofType: "plist")!) as! Array<NSDictionary>
        let questions: Array<Question> = [];
//        for question in questionsArray {
//            let questionWithSound = question.valueForKey("question") as! NSDictionary
//            let choicesWithSound = question.valueForKey("choices") as! Array<NSDictionary>
//            let points = question.valueForKey("points") as! Int
//            questions.append(Question(question: questionWithSound, options: choicesWithSound, points: points))
//        }
        return Quiz(name: "\(name)_\(index)", questions: questions)
    }

}
//
//  TeamPlayerTableViewCell.m
//  Team
//
//  Created by Safad Mohd on 6/10/15.
//  Copyright © 2015 Safad Mohd. All rights reserved.
//

#import "TeamPlayerTableViewCell.h"

@implementation TeamPlayerTableViewCell

- (void)awakeFromNib {
    self.imgViewTeamPlayer.layer.cornerRadius = self.imgViewTeamPlayer.frame.size.height/2.0f;
    self.imgViewTeamPlayer.layer.masksToBounds = YES;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

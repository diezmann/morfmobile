import AVFoundation

class TextToSpeechService : NSObject, AVSpeechSynthesizerDelegate, TextToSpeechServiceDelegate{
    static private let service = TextToSpeechService()
    let speechSynthesizer = AVSpeechSynthesizer()
    var totalUtterances: Int! = 0
    var currentUtterance: Int! = 0
    var totalTextLength: Int = 0
    var spokenTextLengths: Int = 0
    var mediaPlayerView: MorfMediaPlayerView!
    var ttsDelegate: TextToSpeechServiceDelegate? = nil
    
    class func sharedInstance() -> TextToSpeechService {
        return service
    }
    
    override init() {
        super.init()
        speechSynthesizer.delegate = self;
    }
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, didFinishSpeechUtterance utterance: AVSpeechUtterance) {
        spokenTextLengths = spokenTextLengths + utterance.speechString.utf16.count + 1
        
        let progress: Float = Float(spokenTextLengths * 100 / totalTextLength)
        self.ttsDelegate?.didFinishSpeaking()
    }
    
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, didStartSpeechUtterance utterance: AVSpeechUtterance) {
        currentUtterance = currentUtterance + 1
        self.ttsDelegate?.updateProgress(0)
    }
    
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance) {
        let progress: Float = Float(spokenTextLengths + characterRange.location) * 100 / Float(totalTextLength)
        self.ttsDelegate?.updateProgress(progress)
    }
    
    func speak(stringToSpeak: String, delegate: TextToSpeechServiceDelegate) {
        VoiceToTextService.sharedInstance().stopListening()
        if(speechSynthesizer.speaking) {
            speechSynthesizer.stopSpeakingAtBoundary(AVSpeechBoundary.Immediate)
        }
        let speechUtterance = AVSpeechUtterance(string: stringToSpeak)
        speechUtterance.rate = 0.5
        totalTextLength = totalTextLength + stringToSpeak.utf16.count
        speechSynthesizer.speakUtterance(speechUtterance)
        self.ttsDelegate = delegate
    }
    func speak(stringToSpeak: String) {
        speak(stringToSpeak, delegate: self)
    }
    
    func pause() {
        speechSynthesizer.stopSpeakingAtBoundary(AVSpeechBoundary.Immediate)
    }
    
    func updateMediaControl(mediaPlayer: MorfMediaPlayerView!) {
        self.mediaPlayerView = mediaPlayer
    }
    
    func isPlaying() -> Bool {
        return speechSynthesizer.speaking
    }
    
    func didFinishSpeaking() {
    }
    
    func updateProgress(progress: Float) {        
    }
}
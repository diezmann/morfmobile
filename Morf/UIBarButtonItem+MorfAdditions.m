//
//  UIBarButtonItem+MorfAdditions.m
//  Morf
//
//  Created by Tushar on 05/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "UIBarButtonItem+MorfAdditions.h"
#import "LeftBarButtonView.h"

@implementation UIBarButtonItem (MorfAdditions)

+ (UIBarButtonItem *)morfLeftBarButtonWithTitle:(NSString *)title
                                          image:(NSString *)image
                                         target:(id)target
                                         action:(SEL)action {
    LeftBarButtonView *view = [[[NSBundle mainBundle]loadNibNamed:@"LeftBarButtonView" owner:self  options:nil] objectAtIndex:0];
    
    [view setTitle:title image:image];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:target action:action];
    [view addGestureRecognizer:gestureRecognizer];
    
    UIBarButtonItem *barbuttonItem = [[UIBarButtonItem alloc]initWithCustomView:view];
    return barbuttonItem;
}

+ (UIBarButtonItem *)morfRightBarButton {
    UIView *view = [[[NSBundle mainBundle]loadNibNamed:@"RightBarButtonView"
                                                 owner:self
                                               options:nil] objectAtIndex:0];
    UIBarButtonItem *barbuttonItem = [[UIBarButtonItem alloc]initWithCustomView:view];
    return barbuttonItem;
}


+ (UIBarButtonItem *)morfBackBarButtonWithTitle:(NSString *)title
                                         target:(id)target
                                         action:(SEL)action {
    return [self morfLeftBarButtonWithTitle:title
                                      image:@"Back_btn"
                                     target:target
                                     action:action];
}



@end

#import "MorfBaseController.h"
#import "BaseMediaPlayerController.h"
#import "Chapter.h"
#import "Course.h"
#import "CourseContentService.h"
#import "MorfNavigationController.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import <OpenEars/OEFliteController.h>
#import <OpenEars/OEEventsObserver.h>
#import <OpenEars/OEPocketsphinxController.h>
#import <OpenEars/OELanguageModelGenerator.h>
#import <OpenEars/OEAcousticModel.h>
#import <Slt/Slt.h>

#import "BaseTTSViewController.h"
//
//  ProfileActivityCell.swift
//  Morf
//
//  Created by Senthil Kumar on 10/5/15.
//  Copyright © 2015 MorfMedia. All rights reserved.
//

import UIKit

class ProfileActivityCell : UICollectionViewCell {
    @IBOutlet weak var activity : UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        activity.text = ""
    }
    
}
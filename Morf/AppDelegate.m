//
//  AppDelegate.m
//  Morf
//
//  Created by Tushar on 29/04/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "Morf-Swift.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self setupNavBarAppearance];
    
    return YES;
}

-(void)applicationDidBecomeActive:(UIApplication *)application
{
    self.isLoggedOut = YES;
    
    LoginService *service = [[LoginService alloc] init];
    
    NSArray *requestsToRetry = [[NSUserDefaults standardUserDefaults] arrayForKey: @"RequestsToRetry"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey: @"RequestsToRetry"];
    
    // Not the greatest idea in the world, ideally the request should only be removed once success.
    NSString *recordAttempUrl = [NSString stringWithFormat:@"%@QuizAttempt/Record?secret=89U3q1wbvz3805fWlFHVulCEriPR4qF5fUAQmfAl", [AppDelegate baseUrl]];
    for(NSDictionary *request in requestsToRetry) {
        [[[MorfAPIClient alloc] init] executePostRequestWithAuthorization: recordAttempUrl parameters:request success:^(id response) {
        
        } failure:^(NSError *error) {
            
        }];
    }
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

-(void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)setupNavBarAppearance {
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"Nav_Bar_BG.png"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTranslucent:NO];
}

+(User *) currentUser {
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    return appDelegate.user;
}

+(NSString *) baseUrl {
    return [NSString stringWithFormat:@"http://%@.morflearning.com/api/", [[NSUserDefaults standardUserDefaults] stringForKey: @"companyCode"]];
}

+(NSString *) hostUrl {
    return [NSString stringWithFormat:@"http://%@.morflearning.com/", [[NSUserDefaults standardUserDefaults] stringForKey: @"companyCode"]];
}
@end

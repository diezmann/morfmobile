//
//  AppDelegate.h
//  Morf
//
//  Created by Tushar on 29/04/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class User;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) BOOL isLoggedOut;
@property (nonatomic, strong) User *user;
+(User *)currentUser;
+(NSString *) baseUrl;
+(NSString *) hostUrl;

@end


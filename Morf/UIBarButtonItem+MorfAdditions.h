//
//  UIBarButtonItem+MorfAdditions.h
//  Morf
//
//  Created by Tushar on 05/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (MorfAdditions)


+ (UIBarButtonItem *)morfLeftBarButtonWithTitle:(NSString *)title
                                          image:(NSString *)image
                                         target:(id)target
                                         action:(SEL)action;
+ (UIBarButtonItem *)morfRightBarButton;
+ (UIBarButtonItem *)morfBackBarButtonWithTitle:(NSString *)title
                                         target:(id)target
                                         action:(SEL)action;
@end

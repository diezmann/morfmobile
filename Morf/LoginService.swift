//
//  LoginService.swift
//  Morf
//
//  Created by Aditya Athavale on 10/25/15.
//  Copyright © 2015 MorfMedia. All rights reserved.
//

import UIKit

protocol LoginDelegate
{
    func loginFailedWithError(error:NSError)
    func loggedInSuccessfully()
}

class LoginService: NSObject
{
    static var sharedInstance : LoginService = LoginService()
    
    var delegate:LoginDelegate!
    
    func login(username:String, password:String, companyName:String, delegate:LoginDelegate)
    {
        let manager = AFHTTPRequestOperationManager()
        manager.responseSerializer = AFJSONResponseSerializer()

        let url = "\(AppDelegate.baseUrl())Account/Login/\(username)/\(password)"
        
        MorfAPIClient().executeRequest(url, success: { (responseObject) -> Void in
            let user = responseObject as! NSDictionary
            let accessToken = user.valueForKey("access_token") as! String
            let userId = user.valueForKey("Id") as! String
            NSUserDefaults.standardUserDefaults().setValue(accessToken, forKey: "accessToken")
            NSUserDefaults.standardUserDefaults().setValue(userId, forKey: "userId")
            NSUserDefaults.standardUserDefaults().setValue(username, forKey: "userName")
            NSUserDefaults.standardUserDefaults().setValue(password, forKey: "password")
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.isLoggedOut = false
            MorfAPIClient().executeRequestWithAuthorization("\(AppDelegate.baseUrl())/Users/\(userId)", success: { (userDetails: AnyObject) -> Void in
                MorfAPIClient().executeRequestWithAuthorization("\(AppDelegate.baseUrl())/Users/\(userId)/CourseOverviews", success: { (response) -> Void in
                    let overview = response as! NSArray
                    MorfAPIClient().executeRequestWithAuthorization("\(AppDelegate.baseUrl())/Users/\(userId)/Team", success: { (teamResponse) -> Void in
                        ImageLoader().imageForUrl("\(AppDelegate.hostUrl())WebApp/ProfilePictures/\(userId).png",
                            completionHandler: { (image, url) -> () in
                                MorfAPIClient().executeRequestWithAuthorization("\(AppDelegate.baseUrl())Users/\(userId)/RecentActivity?limit=8", success: { (response: AnyObject) -> Void in
                                    self.doneLogin(responseObject as! Dictionary<String, AnyObject>, image: image!, userDetails: userDetails as! Dictionary<String, AnyObject>, overviews: overview as! Array<Dictionary<String, AnyObject>>, team: teamResponse as! Dictionary<String, AnyObject>, response: response)
                                    }) { (error: NSError) -> Void in
                                        self.doneLogin(responseObject as! Dictionary<String, AnyObject>, image: image!, userDetails: userDetails as! Dictionary<String, AnyObject>, overviews: overview as! Array<Dictionary<String, AnyObject>>, team: teamResponse as! Dictionary<String, AnyObject>, response: [])
                                }
                        })
                        }, failure: { (error) -> Void in
                            self.handleFailure(error)
                    })
                    }, failure: { (error) -> Void in
                        self.handleFailure(error)
                })
                }, failure: { (error: NSError) -> Void in
                    self.handleFailure(error)
            })
            }) { (error) -> Void in
                self.handleFailure(error)
        }
    }
    
    private func handleFailure(error: NSError)
    {
        var message = "Invalid username, password and/or company code !"
        if(error.code == -1009) {
            message = "Please check Internet Connection."
        }
        UIAlertView(title: "Login Failed", message: message, delegate: nil, cancelButtonTitle: "Ok").show()
        NSLog("%@", error)
        self.delegate.loginFailedWithError(error)
    }
    
    private func doneLogin(responseObject: Dictionary<String, AnyObject>!, image: UIImage, userDetails: Dictionary<String, AnyObject>, overviews: Array<Dictionary<String, AnyObject>>!, team: Dictionary<String, AnyObject>, response: AnyObject) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let activities = response as! Array<Dictionary<String, AnyObject>>
        let userId = NSUserDefaults.standardUserDefaults().stringForKey("userId")
        let userActivities = activities.filter({$0["UserId"] as? String == userId})
        appDelegate.user = User(response: responseObject, downloadedProfileImage: image, userDetails: userDetails, overviews: overviews, team: team)
        appDelegate.user.activities = userActivities.map({$0["DisplayText"] as! String})
        self.delegate.loggedInSuccessfully()
//        self.goToHomePage()
    }
}

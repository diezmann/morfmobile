import Foundation

class Overview: NSObject {
    var id: String? = ""
    var completion: Int = 0
    var points:Int = 0
    var chapterOverviews = Array<Overview>()
    
    class func overviewWith(overview: Dictionary<String, AnyObject>) -> Overview {
        let item = Overview()
        item.id = overview["CourseId"] as? String
        item.completion = overview["PercentComplete"] as! Int
        item.points = overview["Points"] as! Int
        return item
    }
    
    class func topicOverviewWith(overview:  Dictionary<String, AnyObject>) -> Overview {
        let item = Overview()
        item.id = overview["TopicId"] as? String
        item.completion = overview["PercentComplete"] as! Int
        return item
    }
}

class User : NSObject {
    var accessToken: String
    var id: String
    var companyId: String
    var companyName: String
    var userName: String
    var profileImage: UIImage
    var firstName: String
    var lastName: String
    var score: Int
    var level: Int
    var overviews = Array<Overview>()
    var teamName: String = ""
    var teamPoints: Int = 0
    var activities : Array<String> = []
    var teamId: Int = 0
    var teamRank: Int = 0
    
    init(response: Dictionary<String, AnyObject>!, downloadedProfileImage: UIImage, userDetails: Dictionary<String, AnyObject>, overviews: Array<Dictionary<String, AnyObject>>!, team: Dictionary<String, AnyObject>) {
        accessToken = response["access_token"] as! String
        id = response["Id"] as! String
        companyId = response["companyId"] as! String
        companyName = response["companyName"] as! String
        userName = response["userName"] as! String
        profileImage = downloadedProfileImage
        firstName = userDetails["FirstName"] as! String
        lastName = userDetails["LastName"] as! String
        score = userDetails["Points"] as! Int
        level = userDetails["Level"] as! Int
        teamName = team["Name"] as! String
        teamPoints = team["Points"] as! Int
        teamId = team["Id"] as! Int
        for overview in overviews {
            self.overviews.append(Overview.overviewWith(overview))
        }
    }
    
    func updateOverviews(overviewsResponse: Array<Dictionary<String, AnyObject>>!) {
        self.overviews.removeAll()
        for overview in overviewsResponse {
            overviews.append(Overview.overviewWith(overview))
        }
    }
    
    @objc func overviewForCourse(courseId: String) -> Overview! {
        for overview in overviews {
            if(overview.id == courseId) {
                return overview
            }
        }
        return nil
    }
    
    @objc func overallCompletion() -> Int {
        var total = 0
        for overview in overviews {
            total += overview.completion
        }
        return (total/(overviews.count))
    }
    
    func isChapterComplete(chapterId: String) -> Bool {
        return true;
    }
    
    func fullName() -> String {
        return "\(firstName) \(lastName)"
    }
    
    func rankText() -> String {
        let teamRankString = "\(teamRank)"
        let lastItem = teamRankString[teamRankString.endIndex.predecessor()]
        if(lastItem == "1") {
            return "\(teamRank)st"
        }
        if(lastItem == "2") {
            return "\(teamRank)nd"
        }
        if(lastItem == "3") {
            return "\(teamRank)rd"
        }
        return "\(teamRank)th"
    }
}

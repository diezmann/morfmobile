
#import "HomeController.h"
#import "HomeMenuTableCell.h"
#import "MenuItem.h"
#import "MorfTabBarController.h"
#import "CourseContentService.h"
#import "Morf-Swift.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"

@interface HomeController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong)NSArray *menuItems;

@end

@implementation HomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"accessToken"];
    if(!accessToken) {
        [self.navigationController popToRootViewControllerAnimated: YES];
    }
}

#pragma mark Table View 

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.menuItems.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *reuseIdentifier = (indexPath.row !=1) ? @"HomeMenuLeftCell" : @"HomeMenuRightCell";
    HomeMenuTableCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    [cell populateWithMenuItem:self.menuItems[indexPath.row]];
    if(indexPath.row == 0){
        [cell flash];
    }
    
    return cell
    ;
}

- (CGFloat)tableView:(UITableView *)tableView  heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 142.5;
}

- (void) showCoursesWithResponse: (NSArray *) response forRow: (NSUInteger) row {
    [[CourseContentService sharedInstance] setCoursesWithDict: response];
    MorfTabBarController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
    vc.defaultSelectedIndex = row;
    [self presentViewController:vc animated:YES completion:nil];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row==3){
        [self performSegueWithIdentifier:@"TabBar" sender:indexPath];
    
    }
    
   else if(indexPath.row % 2 == 0){
        MBProgressHUD *progress = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        progress.userInteractionEnabled = NO;
        progress.labelText = indexPath.row == 0 ? @"Loading Courses..." : @"Loading Profile...";
        MorfAPIClient *client = [MorfAPIClient sharedClient];
        NSString *userId = [[NSUserDefaults standardUserDefaults] valueForKey:@"userId"];

        [client executeRequestWithAuthorization:[NSString stringWithFormat:@"%@Users/%@/Courses", [AppDelegate baseUrl], userId] success:^(id response) {
            [progress hide:YES];
            [self showCoursesWithResponse: response forRow:indexPath.row];
        } failure:^(NSError * error) {
            [progress hide: YES];
            NSString * errorDescription = error.userInfo[@"NSLocalizedDescription"];
            if([[errorDescription lowercaseString] containsString: [@"Request failed: unauthorized" lowercaseString]]) {
                [self logout];
                return;
            }
            NSString *message = (error.code == -1009) ? @"Please check Internet Connection." : @"Chapter details not availble!";

            [[[UIAlertView alloc] initWithTitle:@"Error" message:message delegate: nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];

        }];
    }
}


-(void)showTeamScreen{
//    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Team" bundle:[NSBundle mainBundle]];
//    [self presentViewController:[storyBoard instantiateViewControllerWithIdentifier:@"TeamMainViewId"] animated:YES completion:nil];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier  isEqual: @"TabBar"]){
        MorfTabBarController* tabBarController = (MorfTabBarController *)segue.destinationViewController;
        tabBarController.defaultSelectedIndex = [(NSIndexPath *)sender row];
    }
}





#pragma mark getters

- (NSArray *)menuItems {
    if(!_menuItems) {
        NSMutableArray *items = [[NSMutableArray alloc]init];
        
        NSArray *iconImages = @[@"Courses",
                                @"Tutorial",
                                @"Profile",
                                @"Team",
                                ];
        
        NSArray *headers = @[@"Courses",
                             @"Playbook",
                             @"Profile",
                             @"Teams",
                             ];
        
        NSArray *subHeaders = @[@"On more than 20 subjects",
                                @"More than 200 videos",
                                @"Add, edit, delete",
                                @"Your team members",
                                ];
        
        for (NSInteger index = 0; index < headers.count; index ++) {
            MenuItem *menuItem = [MenuItem itemWithHeader:headers[index]
                                                subHeader:subHeaders[index]
                                                iconImage:iconImages[index]];
            [items addObject:menuItem];
        }
        
        _menuItems = items;

    }
    return _menuItems;
}
- (IBAction)searchTapped:(id)sender {
    
    [self.view endEditing:YES];
}




@end

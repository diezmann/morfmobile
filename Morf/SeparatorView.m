//
//  SeparatorView.m
//  Morf
//
//  Created by Tushar on 04/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "SeparatorView.h"

@implementation SeparatorView

- (void)awakeFromNib {
    self.backgroundColor = [UIColor colorWithRed:220 green:220 blue:220 alpha:1];
}

@end

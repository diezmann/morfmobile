//
//  TTSView.swift
//  Morf
//
//  Created by Senthil Kumar on 9/8/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

import Foundation

class TTSView : UIView, TextToSpeechServiceDelegate {
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var progressSlider: UISlider!
    var mediaPlayer = MorfMediaPlayer()
    var textToSpeak: String!

    override func awakeFromNib() {
            self.layer.cornerRadius = 25;
            setupProgressBar()
    }
    
    @IBAction func play() {
        TextToSpeechService.sharedInstance().ttsDelegate = self;
        if(self.textToSpeak == nil) {
            return;
        }
        let imageName = !TextToSpeechService.sharedInstance().isPlaying() ? "Player_Pause" : "Player_Play";
        self.playPauseButton.setImage(UIImage(named: imageName), forState: UIControlState.Normal)
        if(TextToSpeechService.sharedInstance().isPlaying()) {
            self.progressSlider.value = 0
            TextToSpeechService.sharedInstance().pause()            
            return;
        }
        TextToSpeechService.sharedInstance().speak(textToSpeak, delegate: self)
    }

    func setupProgressBar() {
        self.progressSlider.value = 0
        self.progressSlider.setThumbImage(UIImage(named: "Scrubber"), forState: UIControlState.Normal)
    }

    func didFinishSpeaking() {
        self.progressSlider.value = 100
        let imageName = "Player_Play";
        self.playPauseButton.setImage(UIImage(named: imageName), forState: UIControlState.Normal)
        self.progressSlider.value = 0
    }
    
    func updateProgress(progress: Float) {
        self.progressSlider.value = progress
    }
    
    func playFile(fileToPlay: String) {
        mediaPlayer.setPlayBackFile(fileToPlay)
        mediaPlayer.play()
    }
}
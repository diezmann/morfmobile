//
//  Team.h
//  Morf
//
//  Created by Safad Mohd on 7/10/15.
//  Copyright © 2015 MorfMedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Team : NSObject

@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *points;
@property(nonatomic,strong)NSString *percentageComplete;
@property(nonatomic,strong)NSString *teamId;
@property(nonatomic,strong)NSMutableArray *members;


-(Team *)initWithInfoDict:(NSDictionary *)dict;


@end

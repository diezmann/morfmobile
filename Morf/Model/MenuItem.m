//
//  MenuItem.m
//  Morf
//
//  Created by Tushar on 29/04/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "MenuItem.h"

@interface MenuItem()

@property(nonatomic) NSString* header;
@property(nonatomic) NSString* subheader;
@property(nonatomic) NSString* iconImage;

@end

@implementation MenuItem

+ (instancetype)itemWithHeader:(NSString *)header
                     subHeader:(NSString *)subHeader
                     iconImage:(NSString *)image {
    MenuItem *item = [[MenuItem alloc]init];
    
    item.header = [header uppercaseString];
    item.subheader = subHeader;
    item.iconImage = image;
    return item;
}


@end

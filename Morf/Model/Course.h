//
//  Course.h
//  Morf
//
//  Created by Tushar on 02/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Chapter.h"

@interface Course : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *smallDescription;
@property (strong, nonatomic) NSString *longDescription;
@property (strong, nonatomic) NSArray *chapters;
@property (strong, nonatomic) NSString *mediaFileURL;
@property (strong, nonatomic) NSString *welcomeNoteURL;
@property (strong, nonatomic) NSString *courseId;
@property (nonatomic) float completion;
@property (nonatomic) NSUInteger index;
@property (nonatomic) NSString *points;
@property (nonatomic, strong) NSMutableArray *overviews;

+ (Course *)courseFromDictionary:(NSDictionary *)courseDictionary;
- (void)updateOverview: (NSDictionary *) response;
@end

//
//  Course.m
//  Morf
//
//  Created by Tushar on 02/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "Course.h"
#import "Morf-Swift.h"

@implementation Course

+ (Course *)courseFromDictionary:(NSDictionary *)courseDictionary {
    
    Course *course = [[Course alloc]init];
    course.name = courseDictionary[@"Name"];
    course.smallDescription = @"Description Not Available";
    course.longDescription = courseDictionary[@"Exposition"];
    course.courseId = courseDictionary[@"Id"];
    course.overviews = [[NSMutableArray alloc] init];
    NSMutableArray *chapters = [[NSMutableArray alloc]init];
    int chapterNumber = 0;
    for(NSDictionary *chapterData in courseDictionary[@"Topics"]) {
        Chapter *chapter = [Chapter chapterFrom:chapterData courseId:course.courseId];
        chapter.number = [NSNumber numberWithInteger:chapterNumber];
        [chapters addObject:chapter];
        chapterNumber++;
    }
    course.chapters = chapters;
    return course;
}

- (void)updateOverview: (NSArray *) response {
    for(NSDictionary *item in response) {
        [self updateChapterOverview: [Overview topicOverviewWith: item]];
    }
}

-(void)updateChapterOverview: (Overview *) overview {
    for(int i=0; i<self.overviews.count; i++) {
        Overview *o = self.overviews[i];
        if(o.id == overview.id) {
            self.overviews[i] = o;
            break;
        }
    }
    [self.overviews addObject: overview];
    for(Chapter *chapter in self.chapters) {
        if([overview.id isEqualToString: chapter.topicId]) {
            chapter.isCompleted = (overview.completion == 100);
            break;
        }
    }
}

-(Overview *) overviewForChapter: (NSString *) chapterId {
    for(Overview *overview in self.overviews) {
        if([chapterId isEqualToString: overview.id]) {
            return overview;
        }
    }
    return nil;
}

-(float) completion {
    Overview *overview = [[AppDelegate currentUser] overviewForCourse: _courseId];
    return overview.completion;
}


@end

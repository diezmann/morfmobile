
import Foundation

class StringWithSound {
    
    let title: String!
    let audio: String!
    
    init(title: String, audio: String){
        self.title = title
        self.audio = "\(audio).aiff"
    }
    
}

class Choice: NSObject {
    
    var text: StringWithSound
    var isCorrect: Bool
    var choiceId: String!
    
    init(text: String, isCorrect: Bool, choiceId: String){
        self.text = StringWithSound(title: text, audio: "Question_4_Option_1.aiff")
        self.isCorrect = isCorrect
        self.choiceId = choiceId
    }
    
}

//
//  MenuItem.h
//  Morf
//
//  Created by Tushar on 29/04/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuItem : NSObject

@property(nonatomic, readonly) NSString* header;
@property(nonatomic, readonly) NSString* subheader;
@property(nonatomic, readonly) NSString* iconImage;

+ (instancetype)itemWithHeader:(NSString *)header
                     subHeader:(NSString *)subHeader
                     iconImage:(NSString *)image;

@end

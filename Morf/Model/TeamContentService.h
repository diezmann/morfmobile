//
//  TeamContentService.h
//  Morf
//
//  Created by Safad Mohd on 7/10/15.
//  Copyright © 2015 MorfMedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeamContentService : NSObject

+ (NSMutableArray *)getTeams:(id)dict;
+(NSMutableArray *)sortArray:(NSMutableArray *)array;

@end

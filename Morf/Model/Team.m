//
//  Team.m
//  Morf
//
//  Created by Safad Mohd on 7/10/15.
//  Copyright © 2015 MorfMedia. All rights reserved.
//

#import "Team.h"

@implementation Team

-(Team *)initWithInfoDict:(NSDictionary *)dict{
    self.name = [dict valueForKey:@"Name"];
    self.percentageComplete = [dict valueForKey:@"PercentComplete"];
    self.points = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Points"]];
    self.teamId = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Id"]];
    self.members = [dict valueForKey:@"Members"];
    return self;
}


@end

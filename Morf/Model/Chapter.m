//
//  Chapter.m
//  Morf
//
//  Created by Tushar on 04/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "Chapter.h"
#import "AppDelegate.h"

@implementation Chapter

+ (Chapter *)chapterFrom:(NSDictionary *)chapterData courseId:(NSString *)courseId {
    Chapter *chapter = [[Chapter alloc] init];
    chapter.title = chapterData[@"TopicName"];
    chapter.smallDescription = @"Description Not Availbale";
    chapter.imageName = chapterData[@"image"];
    chapter.topicId = chapterData[@"TopicId"];
    chapter.url = [NSString stringWithFormat:@"%@Courses/%@/Topics/%@", [AppDelegate baseUrl], courseId,chapterData[@"TopicId"]];
//    chapter.quiz = [[QuizContentService sharedInstance] quizFor:chapterData[@"quiz"]];
    return chapter;
}

@end


import UIKit

protocol QuizTimeDelegate {
    func timeUpdated(seconds: Int,minutes: Int)
}

class Quiz: NSObject {
   
    var questions: Array<Question> = []
    var name = ""
    private(set) var currentQuestion : Question?
    private(set) var currentQuestionIndex: Int?
    private(set) var isPaused : Bool = true
    private var timeLeft : NSInteger = 300
    private(set) var startTime : NSDate?
    private(set) var endTime : NSDate?
    var isCompleted: Bool {
        get {
            return self.questions.reduce(false) { (allAnswered, question) -> Bool in
                allAnswered && question.isAnswered
            }
        }
    }
    
    var isLastQuestion: Bool {
        get {
            if let question = currentQuestion {
                return self.questions.indexOf(question) == questions.count-1
            }
            return false
        }
    }
    
    private var timer: NSTimer!
    var timeDelegate: QuizTimeDelegate?
    
    var points: Int {
        get {
            let points = self.answeredQuestions.reduce(0) { (sum, question) -> Int in
                sum + question.score
            }
            return points
        }
    }
    
    var answeredQuestions: [Question] {
        get {
            return questions.filter({ (question) -> Bool in
                question.isAnswered
            })
        }
    }
    
    init(name: String, questions: Array<Question>){
        super.init()
        self.name = name
        self.timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "onTimeChanged:", userInfo: nil, repeats: true)
        self.questions = questions
    }
    
    init(quizArray: NSArray) {
        super.init()
        self.name = "abcd"
        self.timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "onTimeChanged:", userInfo: nil, repeats: true)
        for question in quizArray {
            questions.append(Question(questionsDict: question as! NSDictionary))
        }
    }
    
    func resetTime() {
        timeLeft = 300
    }
    
    func startQuiz() {
        startTime = NSDate()
        changeCurrentQuestion(0)
        isPaused = false
    }
    
    func changeCurrentQuestion(index : Int) {
        currentQuestionIndex = index
        currentQuestion = questions[index]
        currentQuestion!.questionStarted()
    }
    
    func nextQuestion() {
        currentQuestionIndex!++
        changeCurrentQuestion(currentQuestionIndex!)
    }
    
    func onTimeChanged(sender : NSTimer) {
        if !isPaused {
            timeLeft--
            let secondsLeft = Int(timeLeft) % 60
            let minutesLeft = Int(timeLeft) / 60
            currentQuestion!.tick()
            self.timeDelegate?.timeUpdated(secondsLeft, minutes: minutesLeft)
        }
    }
    
    func endQuiz() {
        endTime = NSDate()
        isPaused = true
    }
    
    func  togglePause() {
        isPaused = !isPaused
    }
    
    func pause() {
        isPaused = true
    }
    
    func resume() {
        isPaused = false
    }
    
    deinit {
        timer?.invalidate()
    }
}

//
//  Chapter.h
//  Morf
//
//  Created by Tushar on 04/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Quiz;
@interface Chapter : NSObject

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *smallDescription;
@property (strong, nonatomic) NSNumber *number;
@property (strong, nonatomic) NSString *imageName;
@property (strong, nonatomic) Quiz *quiz;
@property (nonatomic) BOOL isCompleted;
@property (strong, nonatomic) NSString *url;
@property (strong, nonatomic) NSString *topicId;
@property (nonatomic) BOOL isLocked;

+ (Chapter *)chapterFrom:(NSDictionary *)chapterData courseId:(NSString *)courseId;

@end

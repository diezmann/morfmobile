
import UIKit

protocol QuestionScoreDelegate {
    func onScoreUpdated(score : Int)
}

class Question: NSObject {
    
    let question: StringWithSound!
    let options: Array<Choice>!
    let questionId: String!
    var selectedOption : Choice?
    private var currentScore : Int = 100
    var score : Int {
        get {
            if currentScore < 5 {
                return 5
            }
            return currentScore
        }
    }
    var scoreDelegate : QuestionScoreDelegate?
    private var incorrectAttempts = 0
    var isAnswered : Bool {
        get {
            return answeredTime != nil
        }
    }
    private(set) var answeredTime : NSDate?
    private(set) var startTime : NSDate?
    private var points = 100
    
    init(questionsDict: NSDictionary) {
        question = StringWithSound(title: questionsDict["QuestionText"] as! String, audio: "Question_4_MoneyLaundering.aiff")
        self.questionId = questionsDict["Id"] as! String
        self.options = Question.createOptions(questionsDict["Choices"] as! Array<NSDictionary>)
        self.selectedOption = nil
    }
    
    private class func createOptions(options: Array<NSDictionary>) -> Array<Choice> {
        var choices:Array<Choice> = []
        for option in options {
            choices.append(Choice(text: option["ChoiceText"] as! String, isCorrect: option["IsCorrect"] as! Bool, choiceId: option["Id"] as! String))
        }
        return choices
    }
    
    func isAnsweredCorrect() -> Bool {
        return selectedOption!.isCorrect
    }
    
    func questionStarted() {
        startTime = NSDate()
    }
    
    func questionAnsweredWrong() {
        incorrectAttempts++
        currentScore = currentScore - 20
        scoreDelegate?.onScoreUpdated(score)
    }
    
    func questionEnded() {
        answeredTime = NSDate()
    }
    
    func parametarize() -> Dictionary<String,AnyObject>  {
        return ["QuestionId": self.questionId, "ChoiceId": self.selectedOption!.choiceId, "Points": self.currentScore, "Incorrect": self.incorrectAttempts, "Correct": self.isAnsweredCorrect(), "Speed": 5, "Date": self.answeredTime!.description]
    }
    
    func tick() {
        currentScore--
        scoreDelegate?.onScoreUpdated(score)
    }
    
}


import UIKit

class TimerHandler: NSObject {

    private var timer: NSTimer
    private var pauseStart: NSDate
    private var previousFireDate: NSDate
    
    init(timer: NSTimer) {
        self.timer = timer
        self.pauseStart = NSDate.distantPast() 
        self.previousFireDate = timer.fireDate
    }
    
    func pause() {
        pauseStart = NSDate(timeIntervalSinceNow: 0)
        previousFireDate = timer.fireDate
        timer.fireDate = NSDate.distantFuture() 
    }
    
    func resume() {
        let pauseTime = -1 * pauseStart.timeIntervalSinceNow
        timer.fireDate = NSDate(timeInterval: pauseTime, sinceDate: previousFireDate)
    }
    
}

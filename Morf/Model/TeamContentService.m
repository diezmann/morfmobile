//
//  TeamContentService.m
//  Morf
//
//  Created by Safad Mohd on 7/10/15.
//  Copyright © 2015 MorfMedia. All rights reserved.
//

#import "TeamContentService.h"
#import "Team.h"
@implementation TeamContentService

+ (NSMutableArray *)getTeams:(id)teamsDict{
    if([teamsDict isKindOfClass:[NSArray class]]){
        teamsDict = [self sortArray:teamsDict];
    }
    NSMutableArray *teams= [[NSMutableArray alloc]init];
    for(NSDictionary *dict in teamsDict){
        Team *team= [[Team alloc]initWithInfoDict:dict];
        team.members = [self sortArray:team.members];
        [teams addObject:team];
    }
    return teams;
}


+(NSMutableArray *)sortArray:(NSMutableArray *)array {
   NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Points" ascending:NO];
   NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
   return  [[array sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
    
}

@end

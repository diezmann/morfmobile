//
//  Cache.swift
//  
//
//  Created by Senthil Kumar on 9/25/15.
//
//

import Foundation
import CoreData

class Cache: NSManagedObject {

    @NSManaged var key: String
    @NSManaged var value: String

}

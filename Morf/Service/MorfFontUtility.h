//
//  MorfFontUtility.h
//  Morf
//
//  Created by Tushar on 05/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MorfFontUtility : NSObject

+ (void)setViewToOpenSansExtraBold:(UIView *)view;
+ (void)setViewToOpenSansBold:(UIView *)view;
+ (void)setViewToOpenSansSemiBold:(UIView *)view;
+ (void)setViewToOpenSansRegular:(UIView *)view;

@end

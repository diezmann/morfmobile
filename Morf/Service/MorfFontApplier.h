//
//  MorfFontApplier.h
//  Morf
//
//  Created by Tushar on 05/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MorfFontApplier : NSObject

@property (nonatomic, weak) IBOutletCollection(UIView) NSArray *viewsToMakeOpenSansBold;
@property (nonatomic, weak) IBOutletCollection(UIView) NSArray *viewsToMakeOpenSansExtraBold;
@property (nonatomic, weak) IBOutletCollection(UIView) NSArray *viewsToMakeOpenSansSemiBold;
@property (nonatomic, weak) IBOutletCollection(UIView) NSArray *viewsToMakeOpenSansLight;
@property (nonatomic, weak) IBOutletCollection(UIView) NSArray *viewsToMakeOpenSansRegular;

@end

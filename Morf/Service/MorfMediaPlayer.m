//
//  MorfAVPlayer.m
//  Morf
//
//  Created by Tushar on 04/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//
#import "MorfMediaPlayer.h"

#define kStatusKey @"Status"

@interface MorfMediaPlayer()

@property (nonatomic, strong)AVPlayerItem *playerItem;
@end


@implementation MorfMediaPlayer


const static NSString* itemStatusContext;

- (void)setPlayBackFile:(NSString *)file {
    

    if(!file)
        return;
    self.currentState = PlayerNotReady;
    ;
    AVURLAsset *asset = [self assetFromFile:file];
    
    NSString *tracksKey = @"tracks";
    __weak MorfMediaPlayer *weakSelf = self;
    
    [asset loadValuesAsynchronouslyForKeys:@[tracksKey] completionHandler:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError *error;
            
            AVKeyValueStatus status = [asset statusOfValueForKey:tracksKey error:&error];
            if(status  == AVKeyValueStatusLoaded) {
                
                self.playerItem = [AVPlayerItem playerItemWithAsset:asset];
                
                [self.playerItem addObserver:weakSelf
                                  forKeyPath:kStatusKey
                 options:NSKeyValueObservingOptionInitial
                 context:&itemStatusContext];
                [[NSNotificationCenter defaultCenter]addObserver:weakSelf
                                                        selector:@selector(playerItemDidReachEnd:)
                                                            name:AVPlayerItemDidPlayToEndTimeNotification
                                                          object:self.playerItem];
                
                [self replaceCurrentItemWithPlayerItem:self.playerItem];
                
            } else {
                NSLog(@"The asset's tracks were not loaded:\n%@", [error localizedDescription]);
            }
            
        });
        
    }];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    
    if (context == &itemStatusContext && self.currentState == PlayerNotReady) {
        dispatch_async(dispatch_get_main_queue(),
                ^{
                    [self.delegate isReadyToPlay];
                    self.currentState = ReadyToPlay;
                           
        });
        return;
    }
    
    return;
}




- (AVURLAsset *)assetFromFile:(NSString *)file {
    NSArray *fileNameComponents = [file componentsSeparatedByString:@"."];
    NSURL *fileURL = [[NSBundle mainBundle]URLForResource:fileNameComponents[0] withExtension:fileNameComponents[1]];
    
    return [AVURLAsset URLAssetWithURL:fileURL options:nil];
}


- (void)playerItemDidReachEnd:(NSObject *)object {
    self.currentState = Paused;
    [self.delegate didFinishPlaying];
}


#pragma mark - Public

- (void)play {
    [super play];
    self.currentState = Playing;
    
}

- (void)pause {
    [super pause];
    self.currentState = Paused;
    
}

- (void)reset {
    [super pause];
    self.currentState = ReadyToPlay;
    [self replaceCurrentItemWithPlayerItem:nil];
}

- (void)moveByTimeInterval:(NSTimeInterval)timeInterval {
    
    if(self.currentState == Playing) {
        NSTimeInterval seekToTime= CMTimeGetSeconds(self.currentItem.currentTime) + timeInterval;
        [self seekToTime:CMTimeMake(seekToTime, 1) completionHandler:^(BOOL finished) {
        }];
    }
}

- (CGFloat)currentProgressInFraction {
    
    return CMTimeGetSeconds(self.currentItem.currentTime)/CMTimeGetSeconds(self.currentItem.duration);
}

#pragma mark - getters

- (PlayerState)currentState {
    
    if(!_currentState)
        _currentState = PlayerNotReady;
    return _currentState;
}

- (void)setPlayerItem:(AVPlayerItem *)playerItem {
    if(_playerItem) {
        [self unsubscribeFromNotifications];
    }
    _playerItem = playerItem;
    
}

- (void)unsubscribeFromNotifications {
    [self.playerItem removeObserver:self forKeyPath:kStatusKey
                            context:&itemStatusContext];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}


-(void)dealloc {

    [self unsubscribeFromNotifications];
}


@end


import Foundation

class DrawingUtility {
    
    class func drawLinesFromCenter(context: CGContext, angles: Array<Double>, centerOfRef: CGPoint) {
        CGContextSaveGState(context)
        CGContextSetStrokeColorWithColor(context, UIColor.blackColor().CGColor);
        CGContextSetLineWidth(context, 0.5);
        for angle in angles {
            drawLineFrom(centerOfRef, endPoint: endPointForAngle(angle, center: centerOfRef), context: context)
        }
        CGContextRestoreGState(context);
    }
    
    class func drawPolygon(context: CGContext, levels: Array<(Double, Double)>, centerOfRef: CGPoint) {
        CGContextSaveGState(context)
        CGContextSetStrokeColorWithColor(context, UIColor.blackColor().CGColor);
        CGContextSetLineWidth(context, 0.5);
        CGContextSetFillColorWithColor(context, UIColor(red: 235/255, green: 183/255, blue: 43/255, alpha: 1).CGColor)
        var startPoint = pointForAngle(levels[0].0, center: centerOfRef, level: levels[0].1)
        CGContextMoveToPoint(context, startPoint.x, startPoint.y)
        for i in 0...levels.count-1 {
            let endPoint = pointForAngle(levels[(i+1)%levels.count].0, center: centerOfRef, level: levels[(i+1)%levels.count].1)
            CGContextAddLineToPoint(context, endPoint.x, endPoint.y)
        }
        CGContextFillPath(context)
        CGContextRestoreGState(context);
    }
    
    private class func drawLineFrom(startPoint: CGPoint, endPoint: CGPoint, context: CGContext) {
        CGContextMoveToPoint(context, startPoint.x, startPoint.y);
        CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
        CGContextStrokePath(context);
    }
    
    class func endPointForAngle(angle: Double, center: CGPoint) -> CGPoint {
        return pointForAngle(angle, center: center, level: 1)
    }
    
    private class func pointForAngle(angle: Double, center: CGPoint, level: Double) -> CGPoint {
        let x = center.x * (1 + CGFloat(cos(M_PI*angle) * level))
        let y = center.y * (1 - CGFloat(sin(M_PI*angle) * level))
        return CGPointMake(x, y)
    }
    
}
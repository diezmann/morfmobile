//
//  MorfFontApplier.m
//  Morf
//
//  Created by Tushar on 05/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "MorfFontApplier.h"

@implementation MorfFontApplier

- (void)setFontNamed:(NSString *)fontName forViews:(NSArray *)views
{
    for (UIView *view in views)
    {
        if ([view isKindOfClass:[UILabel class]])
        {
            UIFont *font = [UIFont fontWithName:fontName size:((UILabel *)view).font.pointSize];
            ((UILabel *)view).font = font;
        }
        else if ([view isKindOfClass:[UIButton class]])
        {
            UIFont *font = [UIFont fontWithName:fontName size:((UIButton *)view).titleLabel.font.pointSize];
            ((UIButton *)view).titleLabel.font = font;
        }
    }
}


- (void)setViewsToMakeOpenSansRegular:(NSArray *)views {
    
    [self setFontNamed:@"OpenSans" forViews:views];
}

- (void)setViewsToMakeOpenSansBold:(NSArray *)viewsToMakeOpenSansBold {
    [self setFontNamed:@"OpenSans-Bold" forViews:viewsToMakeOpenSansBold];
    
}

- (void)setViewsToMakeOpenSansExtraBold:(NSArray *)viewsToMakeOpenSansExtraBold {
        [self setFontNamed:@"OpenSans-ExtraBold" forViews:viewsToMakeOpenSansExtraBold];
}

- (void)setViewsToMakeOpenSansLight:(NSArray *)viewsToMakeOpenSansLight {
        [self setFontNamed:@"OpenSans-Light" forViews:viewsToMakeOpenSansLight];
}

- (void)setViewsToMakeOpenSansSemiBold:(NSArray *)viewsToMakeOpenSansSemiBold {
        [self setFontNamed:@"OpenSans-SemiBold" forViews:viewsToMakeOpenSansSemiBold];
}

@end

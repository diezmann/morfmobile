//
//  ContentService.h
//  Morf
//
//  Created by Tushar on 02/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Course;

@interface CourseContentService : NSObject

@property NSArray *courses;

+ (CourseContentService *)sharedInstance;
- (void)setCoursesWithDict:(NSArray *)coursesArray;
- (BOOL)isAllowedToTakeCourse:(Course *)course;
- (NSUInteger)numberOfCoursesCompleted;
- (float)completion;

@end

//
//  MorfAVPlayer.h
//  Morf
//
//  Created by Tushar on 04/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

typedef enum {
    PlayerNotReady = 0,
    ReadyToPlay,
    Playing,
    Paused,
} PlayerState;

@protocol MorfMediaPlayerDelegate <NSObject>

- (void)didFinishPlaying;
- (void)isReadyToPlay;

@end

@interface MorfMediaPlayer : AVPlayer

- (void)setPlayBackFile:(NSString *)file;
- (void)moveByTimeInterval:(NSTimeInterval)timeInterval;
- (CGFloat)currentProgressInFraction;
- (void)reset;

@property (nonatomic, weak) id<MorfMediaPlayerDelegate> delegate;
@property (nonatomic)PlayerState currentState;

@end

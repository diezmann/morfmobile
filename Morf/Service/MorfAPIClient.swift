
import UIKit

@objc class MorfAPIClient: NSObject, UIAlertViewDelegate {
    
    static private let client = MorfAPIClient()
    
    class func sharedClient() -> MorfAPIClient {
        return client
    }
    
    var failureHandler: (NSError) -> Void = { arg in }
    var persistedError: NSError = NSError(domain: "1", code: 1, userInfo: nil)
    
    func handleFailure(error: NSError, request: NSURLRequest, success: (AnyObject) -> Void, failure: (NSError) -> Void) {
        if let url = request.URL?.absoluteString {
            if(!url.containsString("Logout")) {
                if let errorDescription = (error.userInfo["NSLocalizedDescription"]) {
                    if(errorDescription.lowercaseString.containsString("Request failed: unauthorized".lowercaseString)) {
                        UIAlertView(title: "Error", message: "Your session has expired, or someone has logged into your account on another device.", delegate: self, cancelButtonTitle: "Ok").show()
                        failureHandler = failure
                        persistedError = error
                        return
                    }
                }
            }
            if(error.code != -1009) {
                failure(error)
                return
            }
            if let cachedResponse = readFromCache(url) {
                success(cachedResponse)
                return
            }
        }
        failure(error)
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        failureHandler(persistedError)
    }
    
    func keyFrom(str: String)->String{
        let data = str.dataUsingEncoding(NSUTF8StringEncoding)
        let encodedUrl = data!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        return "cache_\(encodedUrl)"
    }
    
    func readFromCache(url: String) -> AnyObject? {
        let key = keyFrom(url)
        if let storedData = NSUserDefaults.standardUserDefaults().dataForKey(key) {
            return NSKeyedUnarchiver.unarchiveObjectWithData(storedData)
        }
        return nil
    }
    
    func storeInCache(url: String, response: AnyObject) {
        let dataForCache = NSKeyedArchiver.archivedDataWithRootObject(response)
        NSUserDefaults.standardUserDefaults().setValue(dataForCache, forKey: self.keyFrom(url))
    }

    func executeRequest(url: String, success: (AnyObject) -> Void, failure: (NSError) -> Void){
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        let manager = AFHTTPRequestOperationManager()
        let operation = manager.HTTPRequestOperationWithRequest(request, success: { (operation, response) -> Void in
            self.storeInCache(url, response: response)
            success(response)
            }) { (operation, error) -> Void in
                self.handleFailure(error, request: request, success: success, failure: failure)
        }
        operation.start()
    }

    func executeRequestWithAuthorization(url: String, success: (AnyObject) -> Void, failure: (NSError) -> Void){
        let accessToken = NSUserDefaults.standardUserDefaults().valueForKey("accessToken") as! String
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.setValue("bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        let manager = AFHTTPRequestOperationManager()
        let operation = manager.HTTPRequestOperationWithRequest(request, success: { (operation, response) -> Void in
            self.storeInCache(url, response: response)
            success(response)
        }) { (operation, error) -> Void in
            self.handleFailure(error, request: request, success: success, failure: failure)
        }
        operation.start()
    }

    func executePostRequestWithAuthorization(url: String, parameters: Dictionary<String, AnyObject>,success: (AnyObject) -> Void, failure: (NSError) -> Void){
        let accessToken = NSUserDefaults.standardUserDefaults().valueForKey("accessToken") as! String
        let manager = AFHTTPRequestOperationManager()
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.requestSerializer.setValue("bearer \(accessToken)", forHTTPHeaderField:"Authorization");
        let operation = manager.POST(url, parameters: parameters, success: { (operation: AFHTTPRequestOperation!, response: AnyObject!) -> Void in
            self.storeInCache(url, response: response)
            success(response)
            }) { (operation: AFHTTPRequestOperation!, error: NSError!) -> Void in
                if(url.containsString("QuizAttempt/Record?secret=89U3q1wbvz3805fWlFHVulCEriPR4qF5fUAQmfAl")) {
                    var requestsToRetry = NSUserDefaults.standardUserDefaults().arrayForKey("RequestsToRetry");
                    if(requestsToRetry == nil) {
                        requestsToRetry = Array()
                    }
                    requestsToRetry?.append(parameters)
                    NSUserDefaults.standardUserDefaults().setValue(requestsToRetry, forKey: "RequestsToRetry")
                    NSUserDefaults.standardUserDefaults().synchronize()
                }

            self.handleFailure(error, request: operation.request, success: success, failure: failure)
        }
        operation.start()
    }
    
    func executePostNoDataRequestWithAuthorization(url: String, parameters: Dictionary<String, AnyObject>,success: (AnyObject) -> Void, failure: (NSError) -> Void){
        let accessToken = NSUserDefaults.standardUserDefaults().valueForKey("accessToken") as! String
        let manager = AFHTTPRequestOperationManager()
        manager.requestSerializer = AFHTTPRequestSerializer()
        manager.requestSerializer.setValue("bearer \(accessToken)", forHTTPHeaderField:"Authorization");
        
        let operation = manager.POST(url, parameters: parameters, success: { (operation: AFHTTPRequestOperation!, response: AnyObject!) -> Void in
            let cleanedupResponse = response == nil ? [] : response
            self.storeInCache(url, response: cleanedupResponse)
            success(cleanedupResponse)
            }) { (operation: AFHTTPRequestOperation!, error: NSError!) -> Void in
            self.handleFailure(error, request: operation.request, success: success, failure: failure)
        }
        operation.start()
    }

}

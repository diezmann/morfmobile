
import Foundation

class AnimationUtility {
    
    class func spinView(view: UIView, duration: CGFloat, rotations: CGFloat, repetations: Float){
        let animation = CABasicAnimation(keyPath: "transform.rotation.z")
        animation.toValue = CGFloat(M_PI) * 2.0 * rotations * duration
        animation.duration = CFTimeInterval(duration)
        animation.cumulative = true
        animation.repeatCount = repetations
        view.layer.addAnimation(animation, forKey: "rotationAnimation")
    }
    
    class func zoomAndBounce(view: UIView, firstZoomScale: CGFloat,  firstZoomDelay: NSTimeInterval, firstCompletion: (()->Void)?, secondZoomScale: CGFloat, secondZoomDelay: NSTimeInterval, secondCompletion: (()->Void)?) {
        UIView.animateWithDuration(firstZoomDelay, animations: { () -> Void in
            view.transform = CGAffineTransformScale(CGAffineTransformIdentity, firstZoomScale, firstZoomScale)
            }) { (finished) -> Void in
                firstCompletion?()
                UIView.animateWithDuration(secondZoomDelay, animations: { () -> Void in
                    view.transform = CGAffineTransformScale(CGAffineTransformIdentity, secondZoomScale, secondZoomScale)
                    }, completion: { (finished) -> Void in
                        secondCompletion?()
                        view.transform = CGAffineTransformIdentity
                })
        }
    }
    
    class func zoomInOutInfinitely(view: UIView, zoomScale: CGFloat, duration: CFTimeInterval) {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = zoomScale
        animation.duration = duration
        animation.cumulative = false
        animation.repeatCount = HUGE
        animation.autoreverses = true
        view.layer.addAnimation(animation, forKey: "rotationAnimation")
    }
    
    class func springEffectForZoom(view: UIView, zoomLevel:CGFloat) {
        UIView.animateWithDuration(3.0, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 5.0, options: [UIViewAnimationOptions.TransitionNone, .AllowUserInteraction], animations: { () -> Void in
            view.transform = CGAffineTransformScale(CGAffineTransformIdentity, zoomLevel, zoomLevel)
            }, completion: { (finished) -> Void in
                UIView.animateWithDuration(1.0, animations: { () -> Void in
                    view.transform = CGAffineTransformIdentity
                })
            })
    }
    
    class func springEffectForJump(view: UIView, jumpLevel:CGFloat) {
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            view.transform = CGAffineTransformMakeTranslation(0, -jumpLevel)
        }) { (animated) -> Void in
            UIView.animateWithDuration(2.0, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 5.0, options: .TransitionNone, animations: { () -> Void in
                view.transform = CGAffineTransformMakeTranslation(0, 0)
            }, completion: nil)
        }
        
    }
    
}
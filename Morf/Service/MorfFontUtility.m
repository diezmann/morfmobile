//
//  MorfFontUtility.m
//  Morf
//
//  Created by Tushar on 05/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "MorfFontUtility.h"

@implementation MorfFontUtility

+ (void)setFontNamed:(NSString *)fontName forView:(UIView *)view
{
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UIFont *font = [UIFont fontWithName:fontName size:((UILabel *)view).font.pointSize];
        ((UILabel *)view).font = font;
    }
    else if ([view isKindOfClass:[UIButton class]])
    {
        UIFont *font = [UIFont fontWithName:fontName size:((UIButton *)view).titleLabel.font.pointSize];
        ((UIButton *)view).titleLabel.font = font;
    }
}

+ (void) setViewToOpenSansExtraBold:(UIView *)view{
    
    [self setFontNamed:@"OpenSans-ExtraBold" forView:view];
}

+ (void) setViewToOpenSansBold:(UIView *)view {
    [self setFontNamed:@"OpenSans-Bold" forView:view];
}
+ (void) setViewToOpenSansSemiBold:(UIView *)view {
    [self setFontNamed:@"OpenSans-SemiBold" forView:view];
}
+ (void) setViewToOpenSansRegular:(UIView *)view {
    [self setFontNamed:@"OpenSans" forView:view];
}

@end

//
//  ContentService.m
//  Morf
//
//  Created by Tushar on 02/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "CourseContentService.h"
#import "Course.h"
#import "Chapter.h"

static CourseContentService *service;

@implementation CourseContentService

+ (CourseContentService *)sharedInstance {
    if(service == nil){
        service = [[CourseContentService alloc] init];
    }
    return service;
}

- (void)setCoursesWithDict:(NSArray *)coursesArray {
    NSMutableArray *courses = [NSMutableArray arrayWithCapacity:coursesArray.count];
    for (NSDictionary *courseDictionary in coursesArray) {
        Course *course = [Course courseFromDictionary:courseDictionary];
        [courses addObject:course];
    }
    _courses = courses;
}

- (BOOL)isAllowedToTakeCourse:(Course *)course {
    NSUInteger index = course.index;
    return (index == 0) || (index < 3 && [_courses[index-1] completion] == 1);
}

- (NSUInteger)numberOfCoursesCompleted {
    NSUInteger number = 0;
    for (Course *course in _courses) {
        if (course.completion == 100) {
            number++;
        }
    }
    return number;
}

- (float)completion {
    return (float)[self numberOfCoursesCompleted]/_courses.count;
}

@end

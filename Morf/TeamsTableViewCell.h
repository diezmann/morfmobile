//
//  TeamsTableViewCell.h
//  Team
//
//  Created by Safad Mohd on 6/10/15.
//  Copyright © 2015 Safad Mohd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeamsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTeamName;
@property (weak, nonatomic) IBOutlet UILabel *lblTeamPoint;
@property (weak, nonatomic) IBOutlet UILabel *lblTeamRank;

@end

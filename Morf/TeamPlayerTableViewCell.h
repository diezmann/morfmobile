//
//  TeamPlayerTableViewCell.h
//  Team
//
//  Created by Safad Mohd on 6/10/15.
//  Copyright © 2015 Safad Mohd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeamPlayerTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *imgViewTeamPlayer;
@property (weak, nonatomic) IBOutlet UILabel *lblTeamPlayerName;
@property (weak, nonatomic) IBOutlet UILabel *lblteamPlayerPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblteamPlayerRank;

@end

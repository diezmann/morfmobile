//
//  SiriVideoPlayerView.m
//  Morf
//
//  Created by Tushar on 03/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "MorfMediaPlayerView.h"
#import "VideoPlayerView.h"
#import "Morf-Swift.h"

@interface MorfMediaPlayerView() {
    NSTimer *_timer;
    
}

@property (weak, nonatomic) IBOutlet UIImageView *siriImageView;
@property (weak, nonatomic) IBOutlet VideoPlayerView *playerView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UISlider *progressBar;

@end

@implementation MorfMediaPlayerView

static const NSString *itemStatusContext;
static const NSInteger SEEK_BAR_ORIGIN = 0;
static const NSInteger SEEK_BAR_COMPLETE = 1;


- (void)awakeFromNib {
    [super awakeFromNib];
    self.playerView.layer.cornerRadius = 25;
    [self setUpProgressBar];
    self.isPlaying = NO;
    
    [self.playButton setEnabled:NO];
    self.userInteractionEnabled = NO;
}

- (void)enableMediaPlayerView {
    self.userInteractionEnabled = YES;
    [self.playButton setEnabled:YES];
}

- (void) setUpProgressBar {

    self.progressBar.value = SEEK_BAR_ORIGIN;
    [self.progressBar setThumbImage:[UIImage imageNamed:@"Scrubber"]
                           forState:UIControlStateNormal];
}

- (IBAction)seekBackward:(id)sender {
    
    [self.delegate didSeekBack];
}


- (IBAction)seekForward:(id)sender {
    
    [self.delegate didSeekForward];
}


- (IBAction)seekerMoved:(UISlider *)sender {
    
    [self.delegate didSeekToProgressValue:sender.value];
}


- (IBAction)playPauseTapped:(id)sender {
    
    if(self.isPlaying) {
        [self.delegate didPause];
    }
    else {
        [self.delegate didPlay];
        self.isPlaying = YES;
    }
}


#pragma mark - API

- (void)setProgressValue:(CGFloat)value {
    if(value == SEEK_BAR_COMPLETE) {
        self.isPlaying = NO;
    }
        
    self.progressBar.value = value;
}


- (void)resetView {
    
    self.isPlaying = NO;
    [self.progressBar setValue:SEEK_BAR_ORIGIN];
}


#pragma mark 

- (void)setIsPlaying:(BOOL)isPlaying {
    _isPlaying = isPlaying;
    
    NSString *imageName = self.isPlaying ? @"Player_Pause" : @"Player_Play";
    
    [self.playButton setImage:[UIImage imageNamed:imageName]
                     forState:UIControlStateNormal];
    
}

- (void)setSiriImage:(UIImage *)image {
    _siriImageView.image = image;
}

- (void)pauseGame {
    self.playButton.userInteractionEnabled = NO;
}

- (void)resumeGame {
    self.playButton.userInteractionEnabled = YES;
}

@end

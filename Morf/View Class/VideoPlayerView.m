//
//  VideoPlayerView.m
//  Morf
//
//  Created by Tushar on 03/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "VideoPlayerView.h"

@implementation VideoPlayerView

+ (Class)layerClass {
    
    return [AVPlayerLayer class];
}

- (AVPlayer*)player {
    return [ (AVPlayerLayer *)[self layer] player];
}

- (void)setPlayer:(AVPlayer *)player {
    [(AVPlayerLayer *)self.layer setPlayer:player];
}

@end

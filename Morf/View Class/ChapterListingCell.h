//
//  ChapterListingCell.h
//  Morf
//
//  Created by Tushar on 04/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Chapter;

@interface ChapterListingCell : UITableViewCell

@property (nonatomic) Chapter* chapter;
- (void)populateWithChapter:(Chapter *)chapter;

@end

//
//  CourseListingCell.h
//  Morf
//
//  Created by Tushar on 02/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Course;

@interface CourseListingCell : UITableViewCell

- (void)populateWithCourse:(Course *)course;
- (void)flash;

@end


import UIKit

class QuestionNumberView: UIView {

    @IBOutlet weak var numberOne: UIButton!
    @IBOutlet weak var numberTwo: UIButton!
    @IBOutlet weak var numberThree: UIButton!
 
    func changeAttemptedQuestionIndex (index: Int) {
        if(index < 0) {return}
        switch index {
            case 0:
                setAttemptedState(numberOne)
            case 1:
                setAttemptedState(numberTwo)
            default:
                setAttemptedState(numberThree)
        }
    }
    
    func setAttemptedState(button: UIButton) {
        button.layer.cornerRadius = button.frame.size.width/2
        button.setTitleColor(UIColor.blackColor(), forState: .Normal)
        UIView.animateWithDuration(2, animations: { () -> Void in
            button.backgroundColor = UIColor(red: 0, green: 0.6, blue: 0, alpha: 1)
            }){(finished) in
                button.setBackgroundImage(nil, forState: .Normal)
        }
    }
    
}

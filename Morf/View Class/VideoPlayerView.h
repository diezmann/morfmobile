//
//  VideoPlayerView.h
//  Morf
//
//  Created by Tushar on 03/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface VideoPlayerView : UIView

@property (nonatomic) AVPlayer *player;

@end

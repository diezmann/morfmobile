//
//  HomeMenuTableCell.h
//  Morf
//
//  Created by Tushar on 29/04/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MenuItem;

@interface HomeMenuTableCell : UITableViewCell

- (void)populateWithMenuItem:(MenuItem *)item;
- (void)flash;

@end

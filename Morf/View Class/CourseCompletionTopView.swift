

import UIKit

class CourseCompletionTopView: UIView {

    @IBOutlet weak var trophyIcon: UIImageView!
    @IBOutlet weak var scoreTenThousands: UILabel!
    @IBOutlet weak var scoreThousands: UILabel!
    @IBOutlet weak var scoreHundreds: UILabel!
    @IBOutlet weak var scoreTens: UILabel!
    @IBOutlet weak var scoreUnit: UILabel!
    @IBOutlet weak var courseTitle: UILabel!
    
    func showCompletionInfo (course: Course?) {
        courseTitle.text = "\"\(course!.name)\" course successfully"
        NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: Selector("jumpTrophy"), userInfo: nil, repeats: true)
        scoreTenThousands.font = UIFont(name: "DBLCDTempBlack", size: 20)
        scoreThousands.font = UIFont(name: "DBLCDTempBlack", size: 20)
        scoreHundreds.font = UIFont(name: "DBLCDTempBlack", size: 20)
        scoreTens.font = UIFont(name: "DBLCDTempBlack", size: 20)
        scoreUnit.font = UIFont(name: "DBLCDTempBlack", size: 20)
        NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: Selector("updateScore"), userInfo: nil, repeats: false)
    }
    
    func updateScore() {
        let score = Array(String(format: "%05d", AppDelegate.currentUser().teamPoints).characters.reverse())
        showScoreDigit(scoreTenThousands, currentScore: 0, maxScore: Int("\(score[4])")!)
        showScoreDigit(scoreThousands, currentScore: 0, maxScore: Int("\(score[3])")!)
        showScoreDigit(scoreHundreds, currentScore: 0, maxScore: Int("\(score[2])")!)
        showScoreDigit(scoreTens, currentScore: 0, maxScore: Int("\(score[1])")!)
        showScoreDigit(scoreUnit, currentScore: 0, maxScore: Int("\(score[0])")!)
    }
    
    func showScoreDigit(label: UILabel, currentScore: Int, maxScore: Int) {
        NSTimer.scheduledTimerWithTimeInterval(0.085, target: self, selector: Selector("increaseScore:"), userInfo: ["label": label, "score": currentScore, "maxScore": maxScore], repeats: false)
    }
    
    func increaseScore(timer: NSTimer) {
        let label = timer.userInfo?.objectForKey("label") as! UILabel
        let currentScore = timer.userInfo?.valueForKey("score") as! Int
        let maxScore = timer.userInfo?.valueForKey("maxScore") as! Int
        UIView.animateWithDuration(1, animations: { () -> Void in
            label.text = String(format: "%d", currentScore)
        })
        if (currentScore < maxScore) {
            self.showScoreDigit(label, currentScore: currentScore+1, maxScore: maxScore)
        }
    }
    
    func jumpTrophy() {
        AnimationUtility.springEffectForJump(trophyIcon, jumpLevel: 25)
    }

}

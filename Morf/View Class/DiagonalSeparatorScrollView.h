//
//  DiagonalSeparatorView.h
//  Morf
//
//  Created by Tushar on 29/04/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiagonalSeparatorScrollView : UIScrollView <UIScrollViewDelegate>

@property (nonatomic) CGFloat initialHeight;

@end

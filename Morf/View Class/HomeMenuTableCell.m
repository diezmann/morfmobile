//
//  HomeMenuTableCell.m
//  Morf
//
//  Created by Tushar on 29/04/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "HomeMenuTableCell.h"
#import "MenuItem.h"
#import "MorfFontUtility.h"
#import "CourseContentService.h"
#import "AppDelegate.h"
#import "Morf-Swift.h"

@interface HomeMenuTableCell() {

    __weak IBOutlet UIView *completionView;
    __weak IBOutlet UILabel *completionLabel;
    __weak IBOutlet UIImageView *iconImageView;
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UILabel *subTitleLabel;
}

@end

@implementation HomeMenuTableCell

-(void)awakeFromNib {
    [super awakeFromNib];
    
    [MorfFontUtility setViewToOpenSansExtraBold:titleLabel];
    [MorfFontUtility setViewToOpenSansRegular:subTitleLabel];
}

- (void)populateWithMenuItem:(MenuItem *)item {
    titleLabel.text = item.header;
    subTitleLabel.text = item.subheader;
    [[CourseContentService sharedInstance] completion];
    iconImageView.image = [UIImage imageNamed:item.iconImage];
    completionView.hidden = ![item.header isEqualToString:@"COURSES"];
    User *user = [AppDelegate currentUser];
    completionLabel.text = [NSString stringWithFormat:@"%d%%", (int)[user overallCompletion]];
}

- (void)flash {
    self.contentView.backgroundColor = [UIColor clearColor];
    [UIView animateWithDuration:1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut |
     UIViewAnimationOptionRepeat |
     UIViewAnimationOptionAutoreverse |
     UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         self.contentView.backgroundColor = [UIColor colorWithRed:243.0f/255.0f green:193.0f/255.0f blue:0 alpha:1];
                     }
                     completion:nil];
}

@end

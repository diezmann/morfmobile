//
//  SiriVideoPlayerView.h
//  Morf
//
//  Created by Tushar on 03/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "MorfMediaPlayer.h"

@class TextToSpeechService;

@protocol MorfMediaPlayerViewDelegate <NSObject>

- (void)didPlay;
- (void)didPause;
- (void)didSeekToProgressValue:(CGFloat)value;
- (void)didSeekForward;
- (void)didSeekBack;

@end

@interface MorfMediaPlayerView : UIView

@property (weak, nonatomic) id<MorfMediaPlayerViewDelegate> delegate;
@property (weak, nonatomic) TextToSpeechService *ttsService;
@property (nonatomic) BOOL isPlaying;

- (void)enableMediaPlayerView;
- (void)resetView;
- (void)setProgressValue:(CGFloat)value;
- (void)setSiriImage:(UIImage *)image;
- (void)pauseGame;
- (void)resumeGame;

@end

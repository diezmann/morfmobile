//
//  ChapterListingCell.m
//  Morf
//
//  Created by Tushar on 04/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "ChapterListingCell.h"
#import "Chapter.h"
#import "MorfFontUtility.h"

@interface ChapterListingCell()

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *sequenceNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *progressImageView;
@property (weak, nonatomic) IBOutlet UIImageView *lockIcon;
@end

@implementation ChapterListingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [MorfFontUtility setViewToOpenSansBold:_headerLabel];
    [MorfFontUtility setViewToOpenSansBold:_progressLabel];
}

- (void)populateWithChapter:(Chapter *)chapter {
    self.chapter = chapter;
    self.headerLabel.text = chapter.title;
    self.sequenceNumberLabel.text = [NSString stringWithFormat:@"%d", [chapter.number intValue] + 1];
    self.progressLabel.text = chapter.isCompleted ? @"100%" : @"0%";
    self.lockIcon.hidden = !chapter.isLocked;
    self.progressLabel.hidden = chapter.isLocked;
}



@end

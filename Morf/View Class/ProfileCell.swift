
import UIKit

class ProfileCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var levelCompletedLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var badgePointsLabel: UILabel!
    @IBOutlet weak var pointsBadgeView: UIImageView!
    @IBOutlet weak var levelCompletedBadgeView: UIView!
    @IBOutlet weak var crownView: UIImageView!
    @IBOutlet weak var currentLevelView: UIView!
    @IBOutlet weak var activityCollection: UICollectionView!
    
    func update(profile: User) {
        activityCollection.reloadData()
        pointsLabel.text = String(profile.score)
        badgePointsLabel.text = String(profile.score)
        levelCompletedLabel.text = String(format: "%02d",profile.level)
        rankLabel.text = String(format: "%02d",1)
        profileNameLabel.text = "\(profile.firstName) \(profile.lastName)"
        profileImageView.image = profile.profileImage
        
        AnimationUtility.spinView(badgePointsLabel, duration: 1, rotations: 6, repetations: 10)
        AnimationUtility.zoomAndBounce(badgePointsLabel, firstZoomScale: 5.0, firstZoomDelay: 0.7, firstCompletion: { () -> Void in
            self.badgePointsLabel.layer.removeAnimationForKey("rotationAnimation")
            }, secondZoomScale: 1.0, secondZoomDelay: 0.5, secondCompletion: {() -> Void in
                AnimationUtility.spinView(self.pointsBadgeView, duration: 1, rotations: 0.05, repetations: HUGE)
        })
        bounceCrown()
        bounceLevelCompletedBadge()
        bounceCurrentLevelView()
        
        NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: Selector("bounceCrown"), userInfo: nil, repeats: true)
        NSTimer.scheduledTimerWithTimeInterval(6, target: self, selector: Selector("bounceLevelCompletedBadge"), userInfo: nil, repeats: true)
        NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: Selector("bounceCurrentLevelView"), userInfo: nil, repeats: true)
        
    }
    
    func bounceCrown() {
        AnimationUtility.springEffectForJump(crownView, jumpLevel: 30)
    }
    
    func bounceLevelCompletedBadge() {
        AnimationUtility.springEffectForZoom(levelCompletedBadgeView, zoomLevel: 1.5)
    }
    
    func bounceCurrentLevelView() {
        AnimationUtility.springEffectForZoom(currentLevelView, zoomLevel: 1.3)
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ActivityCell", forIndexPath: indexPath) as! ProfileActivityCell
        let index = (indexPath.section*2)+indexPath.row
        if(index < AppDelegate.currentUser().activities.count) {
            cell.activity.text = "\(index +  1))" + AppDelegate.currentUser().activities[index]
        }
        return cell
    }
}

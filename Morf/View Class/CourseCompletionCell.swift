
import UIKit

class CourseCompletionCell: UITableViewCell {

    @IBOutlet weak var topView: CourseCompletionTopView!
    @IBOutlet weak var bottomView: CourseCompletionBottomView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func showCompletionInfo (course: Course?) {
        let user = AppDelegate.currentUser()
        let image = user.profileImage
        profileImage.image = image
        userName.text = user.fullName()
        topView.showCompletionInfo(course)
        bottomView.showCompletionInfo(course)
    }

}

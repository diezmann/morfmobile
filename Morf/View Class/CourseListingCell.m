//
//  CourseListingCell.m
//  Morf
//
//  Created by Tushar on 02/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "CourseListingCell.h"
#import "Course.h"
#import "MorfFontUtility.h"
#import "CourseContentService.h"

@interface CourseListingCell() {
    
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UIImageView *completionLevelImageView;
    __weak IBOutlet UILabel *completionLevel;
    __weak IBOutlet UIView *lockView;
    
}

@end

@implementation CourseListingCell


- (void)awakeFromNib {
    [super awakeFromNib];
    [MorfFontUtility setViewToOpenSansBold:titleLabel];
}
- (void)populateWithCourse:(Course *)course {
    completionLevel.text =  [NSString stringWithFormat:@"%d%%", (int)(course.completion)];
    titleLabel.text = course.name;
    BOOL isAvailable = [[CourseContentService sharedInstance] isAllowedToTakeCourse:course];
    lockView.hidden = isAvailable;
    self.userInteractionEnabled = isAvailable && course.completion != 1;
    self.contentView.backgroundColor = [UIColor clearColor];
}

- (void)flash {
    self.contentView.backgroundColor = [UIColor clearColor];
    [UIView animateWithDuration:1
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut |
     UIViewAnimationOptionRepeat |
     UIViewAnimationOptionAutoreverse |
     UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         self.contentView.backgroundColor = [UIColor colorWithRed:243.0f/255.0f green:193.0f/255.0f blue:0 alpha:1];
                     }
                     completion:nil];
}

@end

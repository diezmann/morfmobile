//
//  DiagonalSeparatorView.m
//  Morf
//
//  Created by Tushar on 29/04/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "DiagonalSeparatorScrollView.h"

@implementation DiagonalSeparatorScrollView


- (void)awakeFromNib {
    self.delegate = self;
}
- (void)drawRect:(CGRect)rect {
    
    [super drawRect:rect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self drawStraightLinesInContext:context];
}

- (void)drawStraightLinesInContext: (CGContextRef)context {
    
    CGFloat height = 50;
    UIGraphicsPushContext(context);
    CGContextBeginPath(context);
    
    CGFloat x_start = 0, x_end = self.layer.bounds.size.width;
    CGFloat y1 = self.initialHeight, y2 =  y1 + height;
    
    CGContextMoveToPoint(context, x_end, y1);
    CGContextAddLineToPoint(context, x_start, y2);
    
    y1 = y2 + height;
    y2 = y1 + height;
    
    CGContextMoveToPoint(context, x_start,y1 );
    CGContextAddLineToPoint(context, x_end, y2);
    
    y1 = y2 + height;
    y2 = y1 + height;
    
    CGContextMoveToPoint(context, x_end, y1);
    CGContextAddLineToPoint(context, x_start, y2);
    
    y1 = y2 + height;
    y2 = y1 + height;
    
    CGContextMoveToPoint(context, x_start,y1 );
    CGContextAddLineToPoint(context, x_end, y2);
    
    y1 = y2 + height;
    y2 = y1 + height;
    
    CGContextMoveToPoint(context, x_end, y1);
    CGContextAddLineToPoint(context, x_start, y2);
    
    [[UIColor lightGrayColor]setStroke];
    CGContextStrokePath(context);
    UIGraphicsPopContext();
    
}


@end

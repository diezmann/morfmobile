
import UIKit

class QuizCellData: UITableViewCell {
    
    var height: CGFloat! = 0.0
    var configBlock: ((UITableViewCell) -> Void)?
    
    init(identifier: String, height: CGFloat){
        self.height = height
        self.configBlock = nil
        super.init(style: .Default, reuseIdentifier: identifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
   
}

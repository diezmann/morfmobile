//
//  LeftBarButtonView.h
//  Morf
//
//  Created by Tushar on 05/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftBarButtonView : UIView

- (void)setTitle:(NSString *)title image:(NSString *)image;

@end

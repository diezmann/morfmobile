
import UIKit

class DashboardProgressView: UIView {

    @IBOutlet weak var circleView: UIImageView!
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        let imageNames: NSArray = ["01","02","03","04","05"]
        let angles = [0.5, 0.9, 1.3, 1.7, 0.1]
        imageNames.enumerateObjectsUsingBlock { (imageName, index, stop) -> Void in
            let image = UIImage(named: imageName as! String)
            let imageView = UIImageView(image: image)
            imageView.center = DrawingUtility.endPointForAngle(angles[index], center: CGPointMake(self.frame.width/2, self.frame.height/2))
            imageView.transform = CGAffineTransformMakeScale(self.frame.width/(6 * image!.size.width), self.frame.height/(6 * image!.size.height))
            self.addSubview(imageView)
        }
        
        self.alpha = 0
        self.transform = CGAffineTransformMakeScale(0.6, 0.6)
        UIView.animateWithDuration(2, delay: 0, options: UIViewAnimationOptions.AllowUserInteraction, animations: { () -> Void in
            self.transform = CGAffineTransformIdentity
            self.alpha = 1
        }, completion: nil)
    }

}

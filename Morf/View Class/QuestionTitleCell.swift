
import UIKit

class QuestionTitleCell: QuizCellData {

    @IBOutlet weak var titleLabel: UILabel!
    var titleText = ""
    func configureWith(title: String){
        UIView.animateWithDuration(1, animations: { () -> Void in
            self.titleLabel.alpha = 0
            self.titleLabel.text = "Q: \(title)"
            self.titleLabel.alpha = 1
        })
    }

}

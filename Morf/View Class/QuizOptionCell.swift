

import UIKit

class QuizOptionCell: QuizCellData {

    @IBOutlet weak var choiceButton: UIButton!
    @IBOutlet weak var choiceDescriptionLabel: UILabel!
    
    var rowIndex: Int!
    var name: String!
    var desc: String!
    var isCorrect: Bool!
    var choice: Choice!
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 3
    }
    
    func isRightChoice() -> Bool {
        return isCorrect
    }
    
    func reset() {
        self.choiceButton.setTitle(name, forState: .Normal)
        self.choiceButton.setBackgroundImage(UIImage(named: "Question_point"), forState: .Normal)
        self.contentView.backgroundColor = UIColor(red: 1, green: 248/255, blue: 215/255, alpha: 1)
        self.choiceDescriptionLabel.text = desc
        self.choiceDescriptionLabel.textColor = UIColor.blackColor()
    }
    
    func configureWith(rowIndex: Int, name: String, desc: String, isCorrect: Bool, choice: Choice){
        self.rowIndex = rowIndex
        self.name = name
        self.desc = desc
        self.isCorrect = isCorrect
        self.choice = choice
        UIView.animateWithDuration(1, animations: { () -> Void in
            self.choiceDescriptionLabel.alpha = 0.2
            self.reset()
            self.choiceDescriptionLabel.alpha = 1
        })
    }

    func markCorrect() {
        self.contentView.backgroundColor = UIColor(red: 0, green: 0.6, blue: 0, alpha: 1)
        choiceButton.setBackgroundImage(UIImage(named: "correct_choice_options"), forState: .Normal)
        choiceDescriptionLabel.textColor = UIColor.whiteColor()
    }
    
    func markWrong() {
        self.contentView.backgroundColor = UIColor(red: 1.0, green: 0.2, blue: 0.2, alpha: 1)
        choiceButton.setBackgroundImage(UIImage(named: "wrong_choice"), forState: .Normal)
        choiceDescriptionLabel.textColor = UIColor.whiteColor()
    }
    
}

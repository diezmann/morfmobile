
import UIKit

protocol DashboardCellDelegate {
    func onlinePlayerSelected(name :String)
}

class DashboardCell: UITableViewCell {

    @IBOutlet weak var firstPlayerView: UIView!
    @IBOutlet weak var secondPlayerView: UIView!
    @IBOutlet weak var progressView: DashboardProgressView!
    
    var delegate: DashboardCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        firstPlayerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: Selector("firstPlayerSelected:")))
        secondPlayerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: Selector("secondPlayerSelected:")))
    }
    
    func firstPlayerSelected(recognizer: UITapGestureRecognizer) {
        delegate?.onlinePlayerSelected("Johny Depp")
    }
    
    func secondPlayerSelected(recognizer: UITapGestureRecognizer) {
        delegate?.onlinePlayerSelected("Brad Pitt")
    }

}



import UIKit

class CourseCompletionBottomView: UIView {

    @IBOutlet weak var pointsEarnedView: UIView!
    @IBOutlet weak var timeTakenView: UIView!
    @IBOutlet weak var coursesCompletedView: UIView!
    @IBOutlet weak var coursePoints: UILabel!
    @IBOutlet weak var coursesCompletedNumber: UILabel!
    @IBOutlet weak var rank: UILabel!
    
    func showCompletionInfo (course: Course?) {
        coursePoints.text = "\(AppDelegate.currentUser().overviewForCourse((course?.courseId)!).points)"
        coursesCompletedNumber.text = "\(CourseContentService.sharedInstance().numberOfCoursesCompleted())"
        rank.text = AppDelegate.currentUser().rankText()
        pointsEarnedView.alpha = 0
        timeTakenView.alpha = 0
        coursesCompletedView.alpha = 0
        UIView.animateWithDuration(3, animations: { () -> Void in
            self.pointsEarnedView.alpha = 1
            self.timeTakenView.alpha = 1
            self.coursesCompletedView.alpha = 1
        })
        NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: Selector("swapPositions"), userInfo: nil, repeats: true)
    }
    
    func swapPositions() {
        let pointsEarnedCenter = pointsEarnedView.center
        UIView.animateWithDuration(0.4, animations: { () -> Void in
            self.pointsEarnedView.center = self.coursesCompletedView.center
            self.coursesCompletedView.center = self.timeTakenView.center
            self.timeTakenView.center = pointsEarnedCenter
        })
    }

}


import UIKit

class QuizHeaderCell: QuizCellData {

    @IBOutlet weak var currentQuestionLabel: UILabel!
    @IBOutlet weak var remainingQuestionsLabel: UILabel!

    func configureWith(currentQuestionNumber: Int, remainingQuestions: Int) {
        currentQuestionLabel.text = String(format: "%02d", currentQuestionNumber)
        let tail = remainingQuestions > 1 ? "Questions" : "Question"
        remainingQuestionsLabel.text = String(format: "%d %@ Remaining", remainingQuestions, tail)
    }
    
}

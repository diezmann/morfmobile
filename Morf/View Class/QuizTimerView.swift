

import UIKit

class QuizTimerView: UIView,QuizTimeDelegate {

    @IBOutlet var minuteLabel: UILabel!
    @IBOutlet var secondsLabel: UILabel!
    
    func timeUpdated(seconds: Int, minutes: Int) {
        secondsLabel.text = String(format: "%02d", seconds)
        minuteLabel.text = String(format: "%02d", minutes)
    }

}

//
//  LeftBarButtonView.m
//  Morf
//
//  Created by Tushar on 05/05/15.
//  Copyright (c) 2015 MorfMedia. All rights reserved.
//

#import "LeftBarButtonView.h"

@interface LeftBarButtonView()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *title;

@end


@implementation LeftBarButtonView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setTitle:@"Back" image:@"Back_btn"];
}

- (void)setTitle:(NSString *)title image:(NSString *)image {
    
    self.imageView.image = [UIImage imageNamed:image];
    self.title.text = title;
}

@end

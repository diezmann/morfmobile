

import UIKit

class DashboardProgressCircleView: UIView {
    var level = Double(arc4random_uniform(7)+3)/10.0
    var levels: Array<Double> = []
    
    var shouldIncrease = true
    var timer: NSTimer?
    
    override func awakeFromNib() {
        levels = [level, level, level, level, level]
        super.awakeFromNib()
        self.layer.masksToBounds = true
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor.blackColor().CGColor
        self.backgroundColor = UIColor(red: 1, green: 250/255, blue: 215/255, alpha: 1)
        if(shouldIncrease){
            self.timer = NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector: Selector("update"), userInfo: nil, repeats: true)
        }
    }
    
    func update() {
        let randIndex = Int(arc4random_uniform(5))
        let incompleteLevels = levels.filter({ (level) -> Bool in
            return level < 1.0
        })
        if(incompleteLevels.isEmpty) {
            timer?.invalidate()
            UIView.animateWithDuration(1, animations: { () -> Void in
                self.backgroundColor = UIColor(red: 235/255, green: 183/255, blue: 43/255, alpha: 1)
            })
            return
        }
        levels[randIndex] += levels[randIndex] < 1 ? 0.1 : 0.0
        setNeedsDisplay()
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        let centerOfRef = CGPointMake(self.frame.width/2, self.frame.height/2)
        UIView.transitionWithView(self, duration: 3, options: [UIViewAnimationOptions.TransitionCrossDissolve, UIViewAnimationOptions.AllowUserInteraction], animations: { () -> Void in
            DrawingUtility.drawPolygon(UIGraphicsGetCurrentContext()!, levels: [(0.1, self.levels[0]), (0.5, self.levels[1]), (0.9, self.levels[2]), (1.3, self.levels[3]), (1.7, self.levels[4]), ], centerOfRef: centerOfRef)
            DrawingUtility.drawLinesFromCenter(UIGraphicsGetCurrentContext()!, angles: [0.1, 0.5, 0.9, 1.3, 1.7], centerOfRef: centerOfRef)
            
        }, completion: nil)
    }

}
